from db_manager import create_table_sqlite
from db_manager import save_order_sqlite
from db_manager import save_new_detail
from settings import DATABASE_TABLE_ORDERS_FIELDS
from os import remove


def main():
    # define database file
    # define
    db_name = 'test_db'
    name = "John"
    phone_number = '+79160306220'
    table_orders = 'orders'
    table_details = 'details'
    num_order = 1
    detail = "Описание"

    columns_order = DATABASE_TABLE_ORDERS_FIELDS

    columns_details = [
        f'num_order',
        f'detail',
    ]

    # create table "orders"
    create_table_sqlite(db_name, table_orders, columns_order)
    # create table "details"
    create_table_sqlite(db_name, table_details, columns_details)
    print("tested CREATE")

    save_order_sqlite(db=db_name,
                      table=table_orders,
                      name=name,
                      phone_number=phone_number,
                      model_name=None,
                      condition=None,
                      trouble=None,
                      pic_path=None,
                      pic_id=None)
    print("Tested INSERT ORDER INTO ORDERS")

    save_new_detail(db=db_name,
                    table=table_details,
                    num_order=num_order,
                    detail=detail)

    print("Tested INSERT DETAIL INTO DETAILS")

    # remove(f"{db_name}.sqlite")


if __name__ == '__main__':
    main()
