# TODO: разнести интерфейсы по разным классам
# интерфейсы:
# -- Смена доступных ролей: основано на правах
# клиент:
# заказ на ремонт - дерево опроса, рахличные кнопки добавление новых заказов, вывод информации по текущим заказам
# Администратор:
# согласование администратора с клиентом по времени
# Колл-центр:
# список обзвона - отчет, изменение статуса, доавление комментариев
# Мастер:
# время до выполнения заказа.
# изменение статуса
# Курьер
# получение списка клиентов сдача/прием
# вывод адресов
#
#


class Interfaces:
    """Добавить сюда нативные интерфейсы"""

    def __init__(self):
        pass

    def create_list_from_db(self, query):
        """
        выполняет произвольный запрос на создание списка к бд
        :param query: any query to db
        :return list: result from db
        """
        pass

    def change_status(self, order, status):
        """
        :param order: int: id order
        :param status: string: new target status
        :return status: string: approved by db new status
        """
        pass

    def commit_changes(self):
        """
        Commit change info by anything
        :return:
        """
        pass

    def confirm_date_to_done(self, order, datestamp):
        """
        The administrator determines the time the order is ready
        :param order: int : id order
        :param datestamp: datestamp obj
        :return datestamp: approved by db
        """
        pass
