#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime

from loguru import logger

from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram import ParseMode, InlineKeyboardMarkup

from settings import DB_NAME as db
from settings import DATABASE_TABLE_CLIENTS_NAME as table_clients

from keyboards.TG_keyboards import questions_kbd
from keyboards.TG_keyboards import hub_keyboard
from keyboards.TG_keyboards import reply_kbd_orders

from db_manager import check_in_staff
from db_manager import get_orders_in_work_list
from db_manager import get_orders_list
from db_manager import get_order_in_orders_by_order_id

from utils.timestamp_parses import readable_data_from_timestamp


# conv_handler constants
entry_phrase = "Новые заказы"
# entry points
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")

# states
AWAIT_SELECTED_ROLE = 'selecting_role'
AWAIT_SELECT_TYPE = 'selecting_type'
SELECTED_NUMBER_ORDER = 'selecting_number_order'

# statuses
CURRENT_STATUS_SUCCESSFUL_END = 'end_ok'


# funcs

def send_message(update, context, message, reply=None, parse_mode=None) -> None:
    """context based send message for any reason
    :param parse_mode:
    :type update: object
    :param context:
    :param message:
    :param reply:

    :return None
    """
    if reply == 'remove':
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            reply_markup=ReplyKeyboardRemove(),
            parse_mode=parse_mode,
        )
    elif reply is None:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            parse_mode=parse_mode,
        )
    else:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            reply_markup=reply,
            parse_mode=parse_mode,
        )

    return None


def cancel(update, context, order_id):
    """
        Текущий заказ помечается как завершенный неоконченный.

    :param order_id:
    :param context:
    :type update: object
    """

    update.message.reply_text(
        'Передумали? Возвращаем в начало...',
        reply_markup=hub_keyboard(chat_id))

    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)

    current_order = get_clients_current_order(db, table_orders, client_id, current_status)

    save_current_status_cancel(
        order_id=order_id,
        status=CURRENT_STATUS_UNSUCCESSFUL_END,
    )

    return ConversationHandler.END


def fun_entry(update, context):
    message = "По номеру или весь список?"

    buttons = [
        "Весь список",
        "По номеру",
        "Отмена",
    ]

    reply_keyboard = questions_kbd(buttons)

    send_message(update, context, message, reply=reply_keyboard)

    return AWAIT_SELECT_TYPE


def fun_select_type(update, context):
    # собираем данные
    chat_id = update.message.chat_id
    users_text = update.message.text.lower()

    # ветвление
    if users_text == "по номеру":
        message = "Отправьте номер заказа"
        send_message(update, context, message)
        return SELECTED_NUMBER_ORDER

    elif users_text == "весь список":
        enlist_new_orders(update, context)
        return ConversationHandler.END

    elif users_text == "отмена":
        reply_keyboard = hub_keyboard(chat_id)
        message = "Отменено"
        send_message(update, context, message=message, reply=reply_keyboard)
        return ConversationHandler.END

    else:
        message = "Выберите один из указанных вариантов!"
        send_message(update, context, message)
        return AWAIT_SELECT_TYPE


def fun_select_order(update, context):
    # константы
    chat_id = update.message.chat_id
    users_text = update.message.text.lower()
    reply_keyboard = hub_keyboard(chat_id)

    # отмена
    if users_text == "отмена":
        reply_keyboard = hub_keyboard(chat_id)
        message = "Отменено"
        send_message(update, context, message=message, reply=reply_keyboard)
        return ConversationHandler.END

    # конвертим в инт
    try:
        users_text = int(users_text)
    except ValueError:
        message = "Нужны цифры, не буквы"
        send_message(update, context, message)
        return SELECTED_NUMBER_ORDER

    # берем дату по заказу
    row, columns = get_order_in_orders_by_order_id(order_id=users_text)
    try:
        current_row = dict(zip(columns, row))
    except TypeError:
        message = "Такого заказа нет."
        send_message(update, context, message=message, reply=reply_keyboard)
        return ConversationHandler.END

    # формируем соощение с готовым запросом
    message = order_message(current_row)

    # статус чек
    available_statuses = [
        "end_ok",
        "in_work",
    ]

    if current_row["status"] not in available_statuses:
        send_message(update, context, message=f"Этот заказ завершён. \n "
                                              f"{current_row['status']}", reply=reply_keyboard)
        return ConversationHandler.END

    # если ок
    inline_keyboard = reply_kbd_orders(order_id=users_text)
    send_message(update, context, message=message, reply=inline_keyboard, parse_mode=ParseMode.HTML)

    message = "Поиск завершен."
    send_message(update, context, message=message, reply=reply_keyboard)

    return ConversationHandler.END


def order_message(row):
    add_data = readable_data_from_timestamp(float(row['timestamp']))

    message = f'<b>№ заказа</b>: {row["order_id"]} \n' \
              f'Добавлен: {add_data} \n' \
              f'<b>Статус</b>: {row["status"]} \n' \
              f' \n' \
              f'<b><i>Информация об аппарате</i></b>\n' \
              f'<i>Производитель</i>: {row["brand_name"]} \n' \
              f'<i>Модель</i>: {row["model_name"]} \n' \
              f'<i>Целевая проблема</i>: {row["trouble"]} \n' \
              f'<i>Комментарий при приеме</i>: {row["comments_receive"]} \n' \
              f'<i>Цена работы</i>: {row["price"]} \n' \
              f'<i>Предоплата</i>: {row["prepayment"]} \n' \
              f' \n' \
              f'<b><i>Информация о клиенте</i></b>\n' \
              f'<i>Имя клиента</i>: {row["client_name"]} \n' \
              f'<i>Номер телефона клиента</i>: \n {row["phone_number"]} \n' \
              f'\n' \
        # f'<i>Исполнитель</i>: \n {master_id}'

    return message


def check_rights(chat_id):
    """ Простая проверка на право вызвать этот список
        TODO: заменить хардлист на список с бд
    """
    approved_roles = ['master',
                      'receiver',
                      'admin',

                      ]

    role = check_in_staff(db=db, table=table_clients, chat_id=chat_id)

    if role in approved_roles:
        return True
    else:
        return False


def enlist_new_orders(update, context):
    """ Выводит список всех новых заказов
    """
    chat_id = update.message.chat_id
    reply_keyboard = hub_keyboard(chat_id)

    if check_rights(chat_id):

        rows, columns = get_orders_list(status=CURRENT_STATUS_SUCCESSFUL_END)
        send_message(update, context,
                     message="Новые доступные заказы: ")
        for row in rows:
            # генерирует клавиатуру с колбэком идентификатора заказа.
            order_id = row[0]
            inline_keyboard = reply_kbd_orders(order_id=order_id)

            # дата добавления заказа
            create_data = datetime.fromtimestamp(float(row[9]), ).strftime('%d.%m.%Y {} %H:%M').format("в")
            message = f'<b>№ заказа</b>: {row[0]} \n' \
                      f'Добавлен: {create_data} \n' \
                      f'<b>Статус</b>: {row[8]} \n' \
                      f' \n' \
                      f'<b><i>Информация об аппарате</i></b>\n' \
                      f'<i>Производитель</i>: {row[12]} \n' \
                      f'<i>Модель</i>: {row[2]} \n' \
                      f'<i>Целевая проблема</i>: {row[4]} \n' \
                      f'<i>Комментарий при приеме</i>: {row[14]} \n' \
                      f'<i>Цена работы</i>: {row[20]} \n' \
                      f'<i>Предоплата</i>: {row[19]} \n' \
                      f' \n' \
                      f'<b><i>Информация о клиенте</i></b>\n' \
                      f'<i>Имя клиента</i>: {row[15]} \n' \
                      f'<i>Номер телефона клиента</i>: \n {row[1]} \n'

            send_message(update, context, message=message, reply=inline_keyboard, parse_mode=ParseMode.HTML)

        message = "Поиск завершен."
        send_message(update, context, message=message, reply=reply_keyboard)

    else:
        message = 'Вы не имеете права на доступ к этой информации.' \
                  ' Отправляю отчет администратору о несанкционированном доступе.'
        send_message(update, context, message=message)

        context.bot.send_message(chat_id='823170382',
                                 message=f'Попытка несанкционированного доступа к списку заказов,'
                                         f' chat_id={chat_id}',
                                 )

    return None


def callback_show_new_orders_tree(update, context):
    """Херня проверить и удалить"""
    # при присоединении в колбэкандлер добавить фильтр
    try:
        query = update.callback_query
        query_data = query.data
        chat_id = query.message.chat_id
        message_id = query.message.id
        inline_message_id = query.inline_message_id
        # добавить проверку на состоятельность order_id
        # проверить входные данные
        #
        if 'change order_id' in query_data:
            order_id = query_data[16:]
            keyboard = [
                # first line of buttons
                [
                    InlineKeyboardButton(f"Завершен", callback_data=f"{order_id}")
                ]
                [
                    InlineKeyboardButton(f"Назад", callback_data=f"{back}")
                ]
                # end of buttons
            ]
            new_reply_markup = InlineKeyboardMarkup(keyboard)
            editMessageReplyMarkup(inline_message_id=inline_message_id,
                                   reply_markup=new_reply_markup)
        elif query_data == back:
            pass

    except Exception as e:
        print(f'{e}')
        logging_exception(f'{e}')

    return None


get_new_orders_handler = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_entry)],

    states={
        AWAIT_SELECT_TYPE: [MessageHandler(Filters.text, fun_select_type)],
        SELECTED_NUMBER_ORDER: [MessageHandler(Filters.text, fun_select_order)],
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=False
)
