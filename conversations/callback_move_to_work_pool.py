#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from time import time

from telegram import ParseMode

from keyboards.TG_keyboards import questions_kbd
from keyboards.TG_keyboards import hub_keyboard

from utils.send_message import send_message_callback
from utils.delete_message import callback_delete_message

from db_manager import check_in_staff
from db_manager import save_selected_role
from db_manager import change_status_order
from db_manager import insert_work_pool
from db_manager import get_client_id
from db_manager import get_order_status

from loguru import logger


def change_work_pool_orders_handler(update, context):
    """
        Перемещает выбранный заказ в пул выполняемой работы

     """
    query = update.callback_query
    # chat_id исполнителя работы, который нажимает эту кнопку
    chat_id = update.effective_chat.id
    client_id = get_client_id(chat_id=chat_id)
    # выдергивает номер заказа из колбэка
    selected_order_id = re.search(r'[\d]{1,15}', query.data).group(0)
    # меняет в таблице заказов статус на рабочий
    current_status_order = get_order_status(order_id=selected_order_id)
    print(current_status_order[0])
    if current_status_order[0] != 'end_ok':
        send_message_callback(update,
                              context,
                              message=f"Заказ №{selected_order_id} уже не доступен!",
                              parse_mode=ParseMode.HTML,
                              )
        callback_delete_message(update, context)
        return None
    print("Послеусловие")
    change_status_order(order_id=selected_order_id,
                        status='in_work',
                        )
    # добавляет в таблицу с выполняемыми работами строку
    insert_work_pool(order_id=selected_order_id,
                     client_id=client_id,
                     status='in_work',
                     timestamp=time(),
                     )
    # отвечает исполнителю о перемещении
    send_message_callback(update,
                          context,
                          message=f"Перемещаю заказ <b>№{selected_order_id}</b> в рабочий пул. \n"
                                  f"Теперь он доступен в рабочем пуле. \n"
                                  f"Для просмотра выполняющихся работ нажмите кнопку 'В работе'",
                          parse_mode=ParseMode.HTML,
                          )
    # Удаляет из списка сообщений свободных заказов сообщение с заказом
    callback_delete_message(update, context)

    # добавляем логику к кнопе в работе
    # выбрать все заказы по чат ид
