#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters

from settings import DB_NAME as db
from settings import DATABASE_TABLE_CLIENTS_NAME as table_clients

from keyboards.TG_keyboards import questions_kbd
from keyboards.TG_keyboards import hub_keyboard

from db_manager import check_in_staff
from db_manager import save_selected_role

from loguru import logger

# conv_handler constants
entry_phrase = "Сменить роль"
# entry points
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")

# states
AWAIT_SELECTED_ROLE = 'selecting_role'


# funcs

def cancel(update, context, order_id):
    """
        Текущий заказ помечается как завершенный неоконченный.

    :param order_id:
    :param context:
    :type update: object
    """

    update.message.reply_text(
        'Передумали? Возвращаем в начало...',
        reply_markup=hub_keyboard(chat_id))

    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)

    current_order = get_clients_current_order(db, table_orders, client_id, current_status)

    save_current_status_cancel(
        order_id=order_id,
        status=CURRENT_STATUS_UNSUCCESSFUL_END,
    )

    return ConversationHandler.END


def send_message(update, context, message, reply=None) -> None:
    """context based send message for any reason
    :type update: object
    :param context:
    :param message:
    :param reply:

    :return None
    """
    if reply == 'remove':
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            reply_markup=ReplyKeyboardRemove())
    elif reply is None:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
        )
    else:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            reply_markup=reply,
        )

    return None


@logger.catch
def fun_on_entry(update, context):
    chat_id = update.message.chat_id
    role = check_in_staff(db, table_clients, chat_id)
    if role != 'client':
        buttons = [
            "Приемщик",
            "Мастер",
        ]
        reply = questions_kbd(buttons)

        message = "Выбери роль ->"

        send_message(update, context,
                     message=message,
                     reply=reply)
        return AWAIT_SELECTED_ROLE


def check_role(update, context):
    chat_id = update.message.chat_id
    answer = update.message.text.lower()
    roles = {
        "мастер": "master",
        "приемщик": "receiver",
        }

    if answer in roles.keys():
        save_selected_role(selected_role=roles[answer], chat_id=chat_id)
        reply_markup = hub_keyboard(chat_id)
        send_message(update, context,
                     message=f"Выбрана роль {answer}", reply=reply_markup)

        return ConversationHandler.END

    else:
        send_message(update, context,
                     message="Белиберда, не понимаю...")
        return AWAIT_SELECTED_ROLE


change_role_handler = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_on_entry)],

    states={
        AWAIT_SELECTED_ROLE: [MessageHandler(Filters.text, check_role)],

    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=False
)

