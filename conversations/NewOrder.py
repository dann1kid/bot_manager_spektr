#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram import ReplyKeyboardMarkup
from telegram import ReplyKeyboardRemove
from telegram import KeyboardButton
from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters

from keyboards.TG_keyboards import questions_kbd
from keyboards.TG_keyboards import hub_keyboard

from Pool import Pool

from db_manager import get_client_id
from db_manager import save_order_1st_question_client
from db_manager import save_order_2nd_question
from db_manager import save_order_3rd_question
from db_manager import save_order_4th_question
from db_manager import save_order_trouble
from db_manager import get_clients_current_order
from db_manager import get_delivery_status
from db_manager import save_geo
from db_manager import save_address
from db_manager import save_current_status

from settings import DB_NAME as db
from settings import DATABASE_TABLE_CLIENTS_NAME as table_clients
from settings import DATABASE_TABLE_ORDERS_NAME as table_orders
from settings import DATABASE_TABLE_ADDRESSES_NAME as table_addresses

# conv_handler constants
entry_phrase = "Новый заказ"
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")

# conv_handler states
CURRENT_STATUS_CHECK_DELIVERY = "delivery_question"
CURRENT_STATUS_BRAND = "brand"
CURRENT_STATUS_MODEL = "model"
CURRENT_STATUS_PROBLEM = "problem"
CURRENT_STATUS_WAIT_COURIER = "await_courier"

# courier branch states
CURRENT_STATUS_AWAIT_COORDS = "coordinates_await"  # add recheck on empty
CURRENT_STATUS_CHECK_COORDS = "coordinates_check"
CURRENT_STATUS_CHECK_ANOTHER_ADDRESS = "address_return_check"  # if no get another address (yes no answer awaiting)
CURRENT_STATUS_CHECK_COORDS_2ND = "coordinates_check_2ND"
CURRENT_STATUS_SUCCESSFUL_END = 'end_ok'
CURRENT_STATUS_UNSUCCESSFUL_END = 'end'

# conversation_handler presets
answer_yes = Filters.regex("^Да$")
answer_no = Filters.regex("^Нет$")


# funcs
def cancel(update, context, current_status):
    """
		Текущий заказ помечается как завершенный неоконченный.

	:param current_status: текущий статус
	:param context:
	:type update: object
	"""
    update.message.reply_text('Передумали? Возвращаем в начало...',
                              reply_markup=hub_keyboard)
    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)

    current_order = get_clients_current_order(db, table_orders, client_id, current_status)

    save_current_status(
        db,
        table_orders,
        client_id,
        order_id=current_order,
        current_status=CURRENT_STATUS_UNSUCCESSFUL_END,
    )

    return ConversationHandler.END


def send_message(update, context, message, reply=None) -> None:
    """context based send message for any reason
    :type update: object
    :param context:
    :param message:
    :param reply:

    :return None
    """
    context.bot.send_message(
        chat_id=update.message.chat_id,
        text=message,
        reply_markup=reply,
    )

    return None


def fun_answer_on_entry(update, context) -> str:
    """
    Отвечает пользователю началом диалога добавления заказа при соотвествующем триггере

    :param update:
    :type context: object
    :return status: str
    """
    print('delivery question')

    user = update.message.from_user
    buttons = [
        "Да",
        "Нет",
    ]
    reply_keyboard = questions_kbd(buttons)
    message = "Заказ отправляется с курьером?"
    send_message(update, context, message=message, reply=reply_keyboard)

    return CURRENT_STATUS_CHECK_DELIVERY


# questions


def fun_first_question_data_phone(update, context) -> str:
    """Опрос клиента об аппарате """
    print('1 question common')
    chat_id = update.message.chat_id
    answer_delivery_status = update.message.text.lower()
    print(answer_delivery_status)

    # yes / no
    if answer_delivery_status not in ("да", "нет"):
        update.message.reply_text('Не поняла... Пожалуйста, ответьте да или нет, или нажмите на кнопки ниже')
        return CURRENT_STATUS_CHECK_DELIVERY  # возврат на этот же стейт, для уверенного ввода пользователя

    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)

    save_order_1st_question_client(client_id=client_id,
                                   delivery=answer_delivery_status,
                                   status=CURRENT_STATUS_BRAND)

    update.message.reply_text(
        'Подскажите бренд вашего аппарата (Например - "Iphone", "Samsung", или вариант "не знаю")',
        reply_markup=ReplyKeyboardRemove()
    )

    return CURRENT_STATUS_BRAND


def fun_second_question_data_phone(update, context) -> str:
    """
		принимает данные с второго ответа
		возвращает след стейт диалога
		так, как  эта функция должна вызыватся из фильтра текста, соотвественно проверок на None не надо
		
	
	:param update:
	:param context:
	:return status: next point of dialogue
	"""
    print('2 question')
    chat_id = update.message.chat_id
    print(chat_id)
    client_id = get_client_id(db, table_clients, chat_id)
    print(client_id)
    current_order = get_clients_current_order(db, table_orders, client_id, CURRENT_STATUS_BRAND)
    print(current_order)
    users_text_brand = update.message.text
    print(users_text_brand)

    update.message.reply_text(
        'Принято, записываю...'
    )

    save_order_2nd_question(db=db,
                            table=table_orders,
                            client_id=client_id,
                            order_id=current_order,
                            brand_name=users_text_brand,
                            current_status=CURRENT_STATUS_MODEL)
    update.message.reply_text(
        f'Записано {users_text_brand}!'
    )
    update.message.reply_text(
        'Подскажите модель вашего аппарата (обычно указан на задней крышке или в настройках, если это телефон)',
    )

    return CURRENT_STATUS_MODEL


def fun_third_question_data_phone(update, context) -> str:
    """
        Принимает данные с третьего ответа
        возвращает следующий стейт
    :param update:
    :param context:
    :return:
    """
    chat_id = update.message.chat_id
    users_text_model = update.message.text

    if users_text_model.lower() == "отмена":
        cancel(update, context, current_status=CURRENT_STATUS_CHECK_COORDS_2ND)
        return ConversationHandler.END

    update.message.reply_text(
        'Принято, записываю...'
    )
    client_id = get_client_id(db, table_clients, chat_id)
    current_order = get_clients_current_order(db, table_orders, client_id, CURRENT_STATUS_MODEL)
    # текущее значение для БД и для смены стейта имеют разные значения,
    # для базы данных оно остается в прошлом, и проверяется в следующем состоянии ДИАЛОГА,
    # которое меняется сейчас!
    save_order_3rd_question(db=db,
                            table=table_orders,
                            client_id=client_id,
                            order_id=current_order,
                            model_name=users_text_model,
                            current_status=CURRENT_STATUS_PROBLEM
                            )
    send_message(update, context, message='В чем заключается проблема с устройством?'
                                          'Расскажите вкратце или пропустите этот вопрос.')

    return CURRENT_STATUS_PROBLEM


def fun_fourth_question_data_phone(update, context):
    print('get trouble')

    users_text_trouble = update.message.text
    chat_id = update.message.chat_id
    client_id = get_client_id(db, table_clients, chat_id)
    current_order = get_clients_current_order(db, table_orders, client_id, CURRENT_STATUS_PROBLEM)

    if users_text_trouble.lower() == "отмена":
        cancel(update, context, current_status=CURRENT_STATUS_CHECK_COORDS_2ND)
        return ConversationHandler.END

    send_message(update, context, message='Сохраняем...')
    # save_order_4th_question(db, table_orders, client_id, current_order, users_text_trouble,
    # current_status=CURRENT_STATUS_PROBLEM)

    # последний вопрос, после которого идет ветвление ->
    return fun_check_delivery(update, context, client_id, current_order, trouble=users_text_trouble, chat_id=chat_id)


def fun_check_delivery(update, context, client_id, current_order, trouble, chat_id):
    """Возвращает векторы веток
    """
    print('check delivery question')

    # необходимо выбрать столбец статуса доставки, который принадлежит этому клиенту
    delivery_status = get_delivery_status(order_id=current_order)

    if delivery_status.lower() == 'да':
        save_order_trouble(db=db,
                           table=table_orders,
                           client_id=client_id,
                           order_id=current_order,
                           trouble=trouble,
                           current_status=CURRENT_STATUS_AWAIT_COORDS,
                           previous_status=CURRENT_STATUS_PROBLEM,
                           )
        send_message(update, context, message='Вы выбрали доставку. ')
        # update.message.reply_text('Вы выбрали доставку. ')
        get_tg_contact_kbd = [[KeyboardButton(text="Отправить геопозицию", request_location=True)]]
        reply = ReplyKeyboardMarkup(get_tg_contact_kbd)

        send_message(update,
                     context,
                     message='Отправьте адрес или геопозицию для приема аппарата курьером',
                     reply=reply)

        return CURRENT_STATUS_AWAIT_COORDS

    else:  # case if without delivery
        save_order_trouble(db=db,
                           table=table_orders,
                           client_id=client_id,
                           order_id=current_order,
                           trouble=trouble,
                           current_status=CURRENT_STATUS_SUCCESSFUL_END,
                           previous_status=CURRENT_STATUS_PROBLEM,
                           )
        send_message(update, context,
                     message=f"Прием заказа завершен. Номер заказа {current_order}",
                     reply=hub_keyboard(chat_id)
                     )
        return ConversationHandler.END


def fun_check_coords(update, context):
    print('1st coord question')
    text_address = update.message.text
    chat_id = update.message.chat_id
    client_id = get_client_id(db, table_clients, chat_id)
    current_order = get_clients_current_order(db, table_orders, client_id, CURRENT_STATUS_AWAIT_COORDS)

    if text_address is not None:
        if text_address.lower() == "отмена":
            cancel(update, context, current_status=CURRENT_STATUS_CHECK_COORDS_2ND)
            send_message(update, context, "Создание заказа отменено")
            return ConversationHandler.END

    try:
        geo = update.message.location
    except TypeError:
        geo = None

    if geo is not None:
        # in case geo send
        print(geo)
        save_geo(db, table_addresses, order_id=current_order, position=1, geo=geo)

    elif geo is None:
        # in case geo not sent
        if text_address is None:
            # in case address and geo is empty
            send_message(update, context, message='Не видно адреса, попробуйте еще раз 😊')
            return CURRENT_STATUS_AWAIT_COORDS
        else:
            # in case geo not send and address not empty
            geo = update.message.text
            print(geo)
            save_address(db, table_addresses, order_id=current_order, position=1, address=geo)

    save_current_status(db,
                        table_orders,
                        client_id,
                        order_id=current_order,
                        current_status=CURRENT_STATUS_CHECK_ANOTHER_ADDRESS,
                        previous_status=CURRENT_STATUS_SUCCESSFUL_END)

    update.message.reply_text('Принято, записываю... 😊')
    # send_message(update, context, 'Принято, записываю... 😊')

    # здесь отправим сообщение с клавиатурой хаба, импортированного из клавиатур
    send_message(update, context, 'Добавление заказа завершено', reply=hub_keyboard(chat_id))

    return CURRENT_STATUS_CHECK_ANOTHER_ADDRESS


order_receive_handler = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_answer_on_entry)],  # здесь выяснять какая роль у chat_id

    # здесь выставить выбор дерева для диалога (pre-states)

    states={

        CURRENT_STATUS_CHECK_DELIVERY: [MessageHandler(Filters.text, fun_first_question_data_phone)],

        CURRENT_STATUS_BRAND: [MessageHandler(Filters.text, fun_second_question_data_phone)],
        CURRENT_STATUS_MODEL: [MessageHandler(Filters.text, fun_third_question_data_phone)],
        CURRENT_STATUS_PROBLEM: [MessageHandler(Filters.text, fun_fourth_question_data_phone)],
        # fun_check_delivery функция должна вернуть стейт
        # в зависимости от выбора юзера с доставкой или без, для направления на соттвествующую ветку
        # курьерская ветка = CURRENT_STATUS_AWAIT_COORDS с кнопками управления адресом
        # без доставки = ConversationHandler.END

        # courier branch
        # 1st address

        # если адреса нет то функция fun_check_coords возвращает этот же стейт, или вопрос о другом адресе
        CURRENT_STATUS_AWAIT_COORDS: [
            MessageHandler(Filters.text | Filters.location, fun_check_coords)],
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=False
)
