# !/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime
from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram import ParseMode

from settings import DB_NAME as db
from settings import DATABASE_TABLE_CLIENTS_NAME as table_clients

from keyboards.TG_keyboards import questions_kbd
from keyboards.TG_keyboards import hub_keyboard
from keyboards.TG_keyboards import reply_kbd_orders
from keyboards.TG_keyboards import inline_kbd_orders_on_delivery
from keyboards.TG_keyboards import inline_kbd_order_to_delivery

from db_manager import check_in_staff
from db_manager import get_orders_in_work_list
from db_manager import get_orders_list
from db_manager import get_orders_on_delivery_by_status
from db_manager import get_orders_in_work_list_by_order_id
from db_manager import get_order_in_work_by_order_id
from db_manager import get_order_in_orders_by_order_id
from db_manager import get_order_on_delivery_by_order_id
from utils.parsers import calculate_geo_index

from utils.timestamp_parses import readable_data_from_timestamp
from utils.send_message import send_message_inline, send_message

from loguru import logger

# conv_handler constants
entry_phrase = "Заказы на выдаче"
# entry points
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")
# states
AWAIT_SELECTED_ROLE = 'selecting_role'
AWAIT_SELECT_TYPE = 'selecting_type'
SELECTED_NUMBER_ORDER = 'selecting_number_order'


def cancel(update, context, order_id):
    """
        Отмена диалога
    :param order_id:
    :param context:
    :type update: object
    """

    update.message.reply_text(
        'Передумали? Возвращаем в начало...',
        reply_markup=hub_keyboard(update.message.chat_id))
    return ConversationHandler.END


def fun_entry(update, context):
    message = "По номеру или весь список?"

    buttons = [
        "Весь список",
        "По номеру",
        "Отмена",
    ]

    reply_keyboard = questions_kbd(buttons)

    send_message_inline(update, context, message, reply=reply_keyboard)

    return AWAIT_SELECT_TYPE


def fun_select_type(update, context):
    chat_id = update.message.chat_id
    users_text = update.message.text.lower()
    if users_text == "по номеру":
        message = "Отправьте номер заказа"
        buttons = ['отмена']
        keyboard = questions_kbd(buttons)
        send_message(update, context, message, reply=keyboard)
        return SELECTED_NUMBER_ORDER

    elif users_text == "весь список":
        enlist_on_delivery_orders(update, context)
        reply_keyboard = hub_keyboard(chat_id)
        message = "Листинг завершен"
        send_message(update, context, message, reply=reply_keyboard)
        return ConversationHandler.END

    elif users_text == "отмена":
        reply_keyboard = hub_keyboard(chat_id)
        message = "Отменено"
        send_message(update, context, message=message, reply=reply_keyboard)
        return ConversationHandler.END

    else:
        message = "Выберите один из указанных вариантов!"
        send_message(update, context, message)
        return AWAIT_SELECT_TYPE


def fun_select_order(update, context):
    # константы
    chat_id = update.message.chat_id
    users_text = update.message.text.lower()
    reply_keyboard = hub_keyboard(chat_id)

    # отмена
    if users_text == "отмена":
        reply_keyboard = hub_keyboard(chat_id)
        message = "Отменено"
        send_message_inline(update, context, message=message, reply=reply_keyboard)
        
        return ConversationHandler.END

    # конвертим в инт
    try:
        users_text = int(users_text)
    except ValueError:
        message = "Нужны цифры, не буквы"
        send_message_inline(update, context, message)

        return SELECTED_NUMBER_ORDER

    # берем дату по заказу
    row, columns = get_order_in_orders_by_order_id(order_id=users_text)
    try:
        current_row = dict(zip(columns, row))
    except TypeError:
        message = "Такого заказа нет."
        send_message_inline(update, context, message=message, reply=reply_keyboard)
        
        return ConversationHandler.END

    # формируем соощение с готовым запросом
    message = order_message(current_row)

    # статус чек
    available_statuses = [
        "on_delivery",
    ]

    if current_row["status"] not in available_statuses:
        send_message_inline(update, context, message=f"Заказ в другом состоянии. \n "
                                                     f"{current_row['status']}", reply=reply_keyboard)
        
        return ConversationHandler.END

    # если ок
    delivery_row, delivery_columns = get_order_on_delivery_by_order_id(order_id=users_text)
    order_on_delivery = dict(zip(delivery_columns, delivery_row))
    print(order_on_delivery)

    inline_kbd = inline_kbd_orders_on_delivery(current_row['order_id'], order_on_delivery['delivery_id'])
    send_message_inline(update, context, message=message, reply=inline_kbd, parse_mode=ParseMode.HTML)

    message = "Поиск завершен."
    send_message_inline(update, context, message=message, reply=reply_keyboard)

    return ConversationHandler.END


def order_message(row):
    add_data = readable_data_from_timestamp(float(row['timestamp']))

    message = f'<b>№ заказа</b>: {row["order_id"]} \n' \
              f'Добавлен: {add_data} \n' \
              f'<b>Статус</b>: {row["status"]} \n' \
              f' \n' \
              f'<b><i>Информация об аппарате</i></b>\n' \
              f'<i>Производитель</i>: {row["brand_name"]} \n' \
              f'<i>Модель</i>: {row["model_name"]} \n' \
              f'<i>Целевая проблема</i>: {row["trouble"]} \n' \
              f'<i>Комментарий при приеме</i>: {row["comments_receive"]} \n' \
              f'<i>Цена работы</i>: {row["price"]} \n' \
              f'<i>Предоплата</i>: {row["prepayment"]} \n' \
              f' \n' \
              f'<b><i>Информация о клиенте</i></b>\n' \
              f'<i>Имя клиента</i>: {row["client_name"]} \n' \
              f'<i>Номер телефона клиента</i>: \n {row["phone_number"]} \n' \
              f'\n' \
        # f'<i>Исполнитель</i>: \n {master_id}'

    return message


def enlist_on_delivery_orders(update, context):
    # Требуется вывести список на доставке
    # взять идентификатор мастера
    # взять данные по заказу
    # вывести в форму сообщения
    # добавить кнопку "выдано" и "принять оплату"
    # и присоединить колбэки с обработкой выдачи данных,
    delivery_rows, delivery_columns = get_orders_on_delivery_by_status(status="on_delivery")
    # делаю словарь для поиска по элементам
    if len(delivery_rows) == 0:
        send_message_inline(update, context, message="На выдаче заказов в данный момент нет 🥺")
    else:
        orders_on_delivery = {}
        for row in delivery_rows:
            # ключ со строкой базы -
            # {order_id: {delivery_id:data, order_id:data_2, etc},
            # }
            orders_on_delivery[row[1]] = dict(zip(delivery_columns, row))
        print(orders_on_delivery)
        # теперь получаем за каждым заказом его данные с таблицы заказов
        list_orders = orders_on_delivery.keys()
        print(list_orders)

        dict_orders = {}
        for key in list_orders:
            print(key)
            row, columns = get_orders_in_work_list_by_order_id(order_id=key)
            dict_orders[key] = dict(zip(columns, row))

        print(dict_orders)
        # берем элементы словаря и распарсиваем в отправляемом сообщении
        for key in list_orders:
            # идентификатор мастера, закончившего работу
            master_id = orders_on_delivery[key]['client_id']
            # адаптируем под человека дату создания заказа
            current_row = dict_orders[key]
            add_data = readable_data_from_timestamp(float(current_row['timestamp']))
            # добавляем инлайн клавиатуру под сообщение с заказом
            inline_kbd = inline_kbd_orders_on_delivery(current_row['order_id'], orders_on_delivery[key]['delivery_id'])
            # дергаем пользовательский статус с рабочей таблицы, чтобы видеть с чем был закончен заказ
            users_status = get_order_in_work_by_order_id(order_id=current_row['order_id'])
            if users_status is None:
                users_status = "Не определено"

            created_geo = calculate_geo_index(client_id=current_row["client_id"])
            # составляем меседж
            message = f'<b>№ заказа</b>: {current_row["order_id"]}{created_geo} \n' \
                      f'Добавлен: {add_data} \n' \
                      f'<b>Статус</b>: {current_row["status"]} \n' \
                      f'<b>Информация о состоянии</b>: {users_status} \n' \
                      f' \n' \
                      f'<b><i>Информация об аппарате</i></b>\n' \
                      f'<i>Производитель</i>: {current_row["brand_name"]} \n' \
                      f'<i>Модель</i>: {current_row["model_name"]} \n' \
                      f'<i>Целевая проблема</i>: {current_row["trouble"]} \n' \
                      f'<i>Комментарий при приеме</i>: {current_row["comments_receive"]} \n' \
                      f'<i>Цена работы</i>: {current_row["price"]} \n' \
                      f'<i>Предоплата</i>: {current_row["prepayment"]} \n' \
                      f' \n' \
                      f'<b><i>Информация о клиенте</i></b>\n' \
                      f'<i>Имя клиента</i>: {current_row["client_name"]} \n' \
                      f'<i>Номер телефона клиента</i>: \n {current_row["phone_number"]} \n' \
                      f'\n' \
                      f'<i>Исполнитель</i>: \n {master_id}'

            send_message_inline(update, context, message=message, reply=inline_kbd, parse_mode=ParseMode.HTML)


get_orders_on_deliver_handler = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_entry)],

    states={
        AWAIT_SELECT_TYPE: [MessageHandler(Filters.text, fun_select_type)],
        SELECTED_NUMBER_ORDER: [MessageHandler(Filters.text, fun_select_order)],
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=False
)
