#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram import ParseMode

from rights.checking import check_rights

from keyboards.TG_keyboards import questions_kbd
from keyboards.TG_keyboards import inline_kbd_orders_in_work
from keyboards.TG_keyboards import hub_keyboard

from db_manager import get_orders_in_work_by_id
from db_manager import get_orders_in_work_list_by_order_id
from db_manager import get_orders_in_work_list
from db_manager import get_clients_masters_id
from db_manager import get_client_id
from db_manager import get_order_in_work_by_order_id
from db_manager import get_data_in_work_by_order_id
from db_manager import get_order_in_orders_by_order_id

from utils.send_message import send_message_inline
from utils.send_message import send_message

from loguru import logger

# conv_handler constants
entry_phrase = "В работе"
# entry points
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")

# states
AWAIT_SELECTED_ROLE = 'selecting_role'
AWAIT_SELECT_TYPE = 'selecting_type'
SELECTED_NUMBER_ORDER = 'selecting_number_order'


def cancel(update, context, order_id):
    """
    команда отмены, обязательный аотруб конверсейшн хэндлера
    :param order_id:
    :param context:
    :type update: object
    """

    send_message(update, context, message="Отмена")

    return ConversationHandler.END


def fun_entry(update, context):
    # конфигурация сообщения
    message = "По номеру или весь список?"

    buttons = [
        "Весь список",
        "По номеру",
        "Отмена",
    ]

    reply_keyboard = questions_kbd(buttons)

    send_message(update, context, message, reply=reply_keyboard)

    return AWAIT_SELECT_TYPE


def fun_select_type(update, context):
    # Собираем данные
    chat_id = update.message.chat_id
    users_text = update.message.text.lower()

    # ветвление
    if users_text == "по номеру":
        message = "Отправьте номер заказа"
        send_message_inline(update, context, message)
        return SELECTED_NUMBER_ORDER

    elif users_text == "весь список":
        enlist_orders_in_work(update, context)
        return ConversationHandler.END

    elif users_text == "отмена":
        reply_keyboard = hub_keyboard(chat_id)
        message = "Отменено"
        send_message_inline(update, context, message=message, reply=reply_keyboard)
        return ConversationHandler.END

    else:
        message = "Выберите один из указанных вариантов!"
        send_message_inline(update, context, message)
        return AWAIT_SELECT_TYPE


def fun_select_order(update, context):
    # константы
    chat_id = update.message.chat_id
    users_text = update.message.text.lower()
    reply_keyboard = hub_keyboard(chat_id)

    # отмена
    if users_text == "отмена":
        message = "Отменено"
        send_message_inline(update, context, message=message, reply=reply_keyboard)
        return ConversationHandler.END

    # конвертим в инт
    try:
        users_text = int(users_text)
    except ValueError:
        message = "Нужны цифры, не буквы"
        send_message_inline(update, context, message)
        return SELECTED_NUMBER_ORDER

    # берем дату по заказу
    row, columns = get_order_in_orders_by_order_id(order_id=users_text)
    try:
        current_row = dict(zip(columns, row))
    except TypeError:
        message = "Такого заказа нет."
        send_message_inline(update, context, message=message, reply=reply_keyboard)
        return ConversationHandler.END

    # формируем соощение с готовым запросом
    message = order_message(current_row)

    # статус чек
    available_statuses = [
        "in_work",
    ]

    if current_row["status"] not in available_statuses:
        send_message_inline(update, context, message=f"Этот заказ завершён. \n "
                                                     f"{current_row['status']}", reply=reply_keyboard)
        return ConversationHandler.END

    # если ок
    work_row, work_columns = get_orders_in_work_by_id(order_id=users_text)
    current_work_row = dict(zip(work_columns, work_row))
    inline_keyboard = inline_kbd_orders_in_work(current_work_row["order_id"], current_work_row["work_id"])
    send_message_inline(update, context, message=message, reply=inline_keyboard, parse_mode=ParseMode.HTML)

    message = "Поиск завершен."
    send_message_inline(update, context, message=message, reply=reply_keyboard)

    return ConversationHandler.END


def order_message(row):
    add_data = readable_data_from_timestamp(float(row['timestamp']))
    # проверяем юзерс статус
    users_status = get_order_in_work_by_order_id(order_id=row["order_id"])
    if users_status is None:
        users_status = "Не указан"

    message = f'<b>№ заказа</b>: {row["order_id"]} \n' \
              f'Добавлен: {add_data} \n' \
              f'<b>Статус</b>: {row["status"]} \n' \
              f'<b>Состояние заказа</b>: {users_status} \n' \
              f' \n' \
              f'<b><i>Информация об аппарате</i></b>\n' \
              f'<i>Производитель</i>: {row["brand_name"]} \n' \
              f'<i>Модель</i>: {row["model_name"]} \n' \
              f'<i>Целевая проблема</i>: {row["trouble"]} \n' \
              f'<i>Комментарий при приеме</i>: {row["comments_receive"]} \n' \
              f'<i>Цена работы</i>: {row["price"]} \n' \
              f'<i>Предоплата</i>: {row["prepayment"]} \n' \
              f' \n' \
              f'<b><i>Информация о клиенте</i></b>\n' \
              f'<i>Имя клиента</i>: {row["client_name"]} \n' \
              f'<i>Номер телефона клиента</i>: \n {row["phone_number"]} \n' \
              f'\n' \
        # f'<i>Исполнитель</i>: \n {master_id}'

    return message


def readable_data_from_timestamp(timestamp):
    return datetime.fromtimestamp(timestamp, ).strftime('%d.%m.%Y {} %H:%M').format("в")


def enlist_orders_in_work(update, context):
    """
        Выводит список заказов в работе, принадлежащих этому мастеру
    :return:
    """

    chat_id = update.message.chat_id
    client_id = get_client_id(chat_id=chat_id)
    if check_rights(chat_id):
        # получаем строки с таблицы в работе и парсим
        work_rows, work_columns = get_orders_in_work_list(client_id=client_id, status='in_work')
        if len(work_rows) == 0:
            send_message_inline(update, context, message="У вас нет выполняемых заказов. \n"
                                                         "Выберите заказы и они будут доступны в этом списке!")

        else:
            # id мастера
            master_id = work_rows[0][2]

            # список всех заказов для этого мастера (order_id)
            orders_ids_list = []
            for row in work_rows:
                orders_ids_list.append(row[1])

            # дикт с заказами этого мастера
            orders_in_work_by_master = {}
            for i in orders_ids_list:
                orders_in_work_by_master[i] = dict(zip(work_columns, work_rows))
            print("orders_in_work_by_master", orders_in_work_by_master)

            # получаем данные по заказам
            dict_orders = {}
            for i in orders_ids_list:
                row, columns = get_orders_in_work_list_by_order_id(order_id=i)
                dict_orders[i] = dict(zip(columns, row))

            print(dict_orders)

            # берем элементы словаря и распарсиваем в отправляемом сообщении
            for key in dict_orders:

                # адаптируем под человека дату создания заказа
                current_row = dict_orders[key]
                add_data = readable_data_from_timestamp(float(current_row['timestamp']))

                # проверяем юзерс статус
                users_status = get_order_in_work_by_order_id(order_id=current_row["order_id"])
                if users_status is None:
                    users_status = "Не указан"

                # добавляем инлайн клавиатуру под сообщение с заказом
                current_order = current_row['order_id']
                current_work_id = orders_in_work_by_master[key]['work_id'][0]
                inline_kbd = inline_kbd_orders_in_work(current_order, current_work_id)
                reply_keyboard = hub_keyboard(chat_id)

                # составляем меседж
                message = f'<b>№ заказа</b>: {current_row["order_id"]} \n' \
                          f'Добавлен: {add_data} \n' \
                          f'<b>Статус</b>: {current_row["status"]} \n' \
                          f'<b>Состояние заказа</b>: {users_status} \n' \
                          f' \n' \
                          f'<b><i>Информация о телефоне</i></b>\n' \
                          f'<i>Производитель</i>: {current_row["brand_name"]} \n' \
                          f'<i>Модель</i>: {current_row["model_name"]} \n' \
                          f'<i>Целевая проблема</i>: {current_row["trouble"]} \n' \
                          f'<i>Комментарий при приеме</i>: {current_row["comments_receive"]} \n' \
                          f'<i>Цена работы</i>: {current_row["price"]} \n' \
                          f'<i>Предоплата</i>: {current_row["prepayment"]} \n' \
                          f' \n' \
                          f'<b><i>Информация о клиенте</i></b>\n' \
                          f'<i>Имя клиента</i>: {current_row["client_name"]} \n' \
                          f'<i>Номер телефона клиента</i>: \n {current_row["phone_number"]} \n' \
                          f'\n' \
                          f'<i>Исполнитель</i>: \n {master_id}'

                send_message_inline(update, context, message=message, reply=inline_kbd, parse_mode=ParseMode.HTML)
            send_message_inline(update, context, message="Поиск завершен", reply=reply_keyboard)


get_in_work_orders_handler = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_entry)],

    states={
        AWAIT_SELECT_TYPE: [MessageHandler(Filters.text, fun_select_type)],
        SELECTED_NUMBER_ORDER: [MessageHandler(Filters.text, fun_select_order)],
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=False
)
