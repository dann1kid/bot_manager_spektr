#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram import ParseMode

from rights.checking import check_rights

from keyboards.TG_keyboards import inline_kbd_orders_in_work
from keyboards.TG_keyboards import inline_kbd_detail_on_delivery

from db_manager import get_orders_in_work_by_id
from db_manager import get_orders_in_work_list_by_order_id
from db_manager import get_orders_in_work_list
from db_manager import get_clients_masters_id
from db_manager import get_client_id
from db_manager import get_details_list_by_status
from db_manager import get_order_in_work_by_order_id
from utils.parsers import calculate_geo_index

from utils.send_message import send_message
from utils.send_message import send_message_inline

from loguru import logger

# conv_handler constants
entry_phrase = "Заказы на детали"
# entry points
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")


def cancel(update, context, order_id):
    """
    команда отмены, обязательный аотруб конверсейшн хэндлера
    :param order_id:
    :param context:
    :type update: object
    """

    update.message.reply_text(
        'Отмена',
        reply_markup=hub_keyboard(chat_id))

    return ConversationHandler.END


def readable_data_from_timestamp(timestamp):
    return datetime.fromtimestamp(timestamp, ).strftime('%d.%m.%Y {} %H:%M').format("в")


def enumerate_details_orders(update, context):
    chat_id = update.message.chat_id
    client_id = get_client_id(chat_id=chat_id)
    if check_rights(chat_id):
        detail_rows, detail_columns = get_details_list_by_status(status='end_ok')
        try:
            length = len(detail_rows)
        except TypeError:
            length = 0
            # не работает в случае исключения, поэтому приводим к ифу
        if length == 0:
            send_message(update, context, message="Нет заказанных деталей")
        else:
            dict_details = {}
            for row in detail_rows:
                dict_details[row[0]] = dict(zip(detail_columns, row))
            print(dict_details)

            for key in dict_details:
                current_row = dict_details[key]
                add_data = readable_data_from_timestamp(float(current_row['timestamp']))
                inline_kbd = inline_kbd_detail_on_delivery(current_row['detail_id'],)
                created_geo = calculate_geo_index(client_id=current_row["client_id"])
                message = f'<b>№ заказа</b>: {current_row["detail_id"]}{created_geo} \n' \
                          f'Добавлен: {add_data} \n' \
                          f'<b>Состояние заказа</b>: {current_row["users_status"]} \n' \
                          f' \n' \
                          f'<b><i>Информация о детали</i></b>\n' \
                          f'<i>Деталь </i>: {current_row["detail_name"]} \n' \
                          f'<i>Цена и нюансы </i>: {current_row["detail_text"]} \n' \
                          f'<i>Предоплата</i>: {current_row["prepayment"]} \n' \
                          f' \n' \
                          f'<b><i>Информация о клиенте</i></b>\n' \
                          f'<i>Имя клиента</i>: {current_row["client_name"]} \n' \
                          f'<i>Номер телефона клиента</i>: \n {current_row["phone_number"]} \n' \
                          f'\n' \
                          f'<i>Кто принял</i>: \n {current_row["client_id"]}'

                send_message_inline(update, context, message=message, reply=inline_kbd, parse_mode=ParseMode.HTML)


get_list_details_orders = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, enumerate_details_orders)],

    states={
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=False
)
