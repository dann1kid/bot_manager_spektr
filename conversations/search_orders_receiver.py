#!/usr/bin/env python
# -*- coding: utf-8 -*-


from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram import ParseMode

from db_manager import get_order_in_orders_by_order_id, full_search_sqlite_in_orders

from utils.cancel import cancel

from keyboards.TG_keyboards import hub_keyboard, inline_kbd_detail_full_search
from keyboards.TG_keyboards import questions_kbd

# conversation_handler constants
from utils.send_message import send_message_inline, send_message
from utils.timestamp_parses import readable_data_from_timestamp

entry_phrase = "Поиск по всем заказам"
# entry points
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")

BRANCH = "branch"
SEARCHING_NUMBER = "search_num_order"
FULLSEARCH = 'fullsearch'


def order_message(row: dict):
    """
    ФОрматирует сообщение под передаваемый словать
    :param row: dict
    :return: string : Форматированная строка
    """
    add_data = readable_data_from_timestamp(float(row['timestamp']))

    message = f'<b>№ заказа</b>: {row["order_id"]} \n' \
              f'Добавлен: {add_data} \n' \
              f'<b>Статус</b>: {row["status"]} \n' \
              f' \n' \
              f'<b><i>Информация об аппарате</i></b>\n' \
              f'<i>Производитель</i>: {row["brand_name"]} \n' \
              f'<i>Модель</i>: {row["model_name"]} \n' \
              f'<i>Целевая проблема</i>: {row["trouble"]} \n' \
              f'<i>Комментарий при приеме</i>: {row["comments_receive"]} \n' \
              f'<i>Цена работы</i>: {row["price"]} \n' \
              f'<i>Предоплата</i>: {row["prepayment"]} \n' \
              f' \n' \
              f'<b><i>Информация о клиенте</i></b>\n' \
              f'<i>Имя клиента</i>: {row["client_name"]} \n' \
              f'<i>Номер телефона клиента</i>: \n {row["phone_number"]} \n' \
              f'\n' \

    return message


def fun_entry(update, context):
    message = "Выбери поиск"
    buttons = [
        "По номеру заказа",
        "Общий поиск",
        "Отмена"
    ]
    reply = questions_kbd(buttons)
    send_message_inline(update, context, message=message, reply=reply)
    return BRANCH


def fun_branch(update, context):
    users_text = update.message.text.lower()
    chat_id = update.message.chat_id

    if users_text == "по номеру заказа":
        message = "Отправьте номер для поиска"
        send_message(update, context, message=message, reply='remove')
        return SEARCHING_NUMBER
    elif users_text == "общий поиск":
        message = "Напишите ключевые слова для поиска через пробел. \n" \
                  "Например: Honor Дмитрий."
        send_message(update, context, message=message, reply='remove')
        return FULLSEARCH
    elif users_text == "отмена":
        update.message.reply_text(
            'Возвращаю в начало',
            reply_markup=hub_keyboard(chat_id))
        return ConversationHandler.END

    else:
        message = "Попробуй еще раз."
        send_message(update, context, message=message)
        return BRANCH


def search_by_num(update, context):
    """
    возвращает форматированное сообщение по номеру заказа
    :param update:
    :param context:
    :return:
    """
    chat_id = update.message.chat_id
    users_text = update.message.text.lower()

    if users_text == "отмена":
        cancel_by_message(users_text, update, context)
        return ConversationHandler.END

    row, columns = get_order_in_orders_by_order_id(order_id=users_text)
    current_row = dict(zip(columns, row))

    reply = hub_keyboard(chat_id)
    message = order_message(current_row)
    send_message_inline(update, context, message=message, parse_mode=ParseMode.HTML)

    send_message(update, context, message='Поиск окончен', reply=reply)

    return ConversationHandler.END


def replace_substring_and(text):
    """
    Удаляет пробелы и добавляет AND для полнтоекстового запроса в бд
    :param text: пользовательский ввод
    :return: форматированную строку
    """
    return text.replace(' ', ' AND ')


def full_search_message(rows: list):
    """
    ФОрматирует сообщение под передаваемый список
    :param rows: list
    :return: string : Форматированная строка
    """

    # Инициируем конечные переменные - сообщение: str,  и крепимые к нему кнопки[str,]
    order_ids = []
    message = ""

    for row in rows:
        add_data = readable_data_from_timestamp(float(row['timestamp']))
        string_row = f"№{row['order_id']}, " \
                     f"Номер{row['phone_number']}, " \
                     f"Имя {row['client_name']}, " \
                     f"обратился с проблемой: {row['trouble']}, " \
                     f"{add_data}\n"
        message += string_row
        order_ids.append(f"{row['order_id']}")

    reply = inline_kbd_detail_full_search(order_ids)

    if (message is None) or (message == ""):
        message = "Заказов не найдено"

    return message, reply


def cancel_by_message(users_text, update, context):
    """
    В случае если человек нажал на репли "отмена"
    :param users_text:
    :param update:
    :param context:
    :return:
    """
    message = "Отменено"
    reply = hub_keyboard(update.message.chat_id)
    send_message(update, context, message=message, reply=reply)


def fulltext_search(update, context):
    chat_id = update.message.chat_id
    users_text = update.message.text.lower()
    formatted_text = replace_substring_and(users_text)

    # get data
    data, columns = full_search_sqlite_in_orders(request=formatted_text)

    if users_text == "отмена":
        cancel_by_message(users_text, update, context)
        return ConversationHandler.END

    # reformat data
    rows = []
    for row in data:
        rows.append(dict(zip(columns, row)))

    if len(rows) >= 5:
        message = "Результатов больше 5 совпадений. \n" \
                  "Я покажу их, но лучше если вы уточните информацию."
        send_message_inline(update, context, message)

    # показываю срез из 5 строк
    # send_message_inline (update, context, full_search_message(row), parse_mode=ParseMode.HTML)
    message, inline_kbd = full_search_message(rows[:5])
    send_message_inline(update, context, message, reply=inline_kbd)
    message = "Поиск окончен."
    reply = hub_keyboard(chat_id)
    send_message(update, context, message=message, reply=reply)

    return ConversationHandler.END


search_all_orders_receiver_handler = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_entry)],

    states={
        BRANCH: [MessageHandler(Filters.text, fun_branch)],
        SEARCHING_NUMBER: [MessageHandler(Filters.text, search_by_num)],
        FULLSEARCH: [MessageHandler(Filters.text, fulltext_search)],
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=False,
)
