#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram import ParseMode

from rights.checking import check_rights

from keyboards.TG_keyboards import inline_kbd_orders_in_work

from db_manager import get_orders_in_work_by_id
from db_manager import get_orders_in_work_list_by_order_id
from db_manager import get_orders_in_work_list
from db_manager import get_clients_masters_id
from db_manager import get_client_id
from db_manager import get_order_in_work_by_order_id

from utils.send_message import send_message_inline
from utils.send_message import send_message

from loguru import logger

# conv_handler constants
entry_phrase = "тест"
# entry points
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")


def cancel(update, context, order_id):
    """
    команда отмены, обязательный аотруб конверсейшн хэндлера
    :param order_id:
    :param context:
    :type update: object
    """

    update.message.reply_text(
        'Отмена',
        reply_markup=hub_keyboard(chat_id))

    return ConversationHandler.END


def readable_data_from_timestamp(timestamp):
    return datetime.fromtimestamp(timestamp, ).strftime('%d.%m.%Y {} %H:%M').format("в")


class Replics:
    def __init__(self):
        self.global_var_1 = ""
        self.global_var_2 = ""
        self.global_var_3 = ""

    def entry(self, update, context):
        send_message(update, context, message="Отправь любое сообщение")
        return "from_return"

    def first_fun(self, update, context):
        users_text = update.message.text
        self.global_var_1 = users_text
        update.message.reply_text(f"Отправил текст {users_text}")
        return "from_first"

    def second_fun(self, update, context):
        users_text = update.message.text
        self.global_var_2 = users_text
        update.message.reply_text(f"Отправил текст {self.global_var_1}, {self.global_var_2}")
        return "from_second"

    def third_fun(self, update, context):
        users_text = update.message.text
        self.global_var_3 = users_text
        update.message.reply_text(f"Отправил текст {self.global_var_1}, {self.global_var_2}, {self.global_var_3}")
        return ConversationHandler.END


replies = Replics()

test_globals_handler = ConversationHandler(

    entry_points=[MessageHandler(ENTRY_POINT, replies.entry)],

    states={
        "from_return": [MessageHandler(Filters.text, replies.first_fun)],
        "from_first": [MessageHandler(Filters.text, replies.second_fun)],
        "from_second": [MessageHandler(Filters.text, replies.third_fun)],
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=False

)
