# !/usr/bin/env python
# -*- coding: utf-8 -*-
import re

from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import CallbackQueryHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram import ParseMode

from db_manager import save_comments_statement_in_db
from db_manager import get_order_id_on_commenting
from db_manager import save_comments_in_work_db
from keyboards.TG_keyboards import hub_keyboard, questions_kbd

from utils.send_message import send_message_callback
from utils.send_message import send_message

entry_point_pattern = "^take comments"
TAKE_COMMENT = "taking_comment"


def cancel(update, context, order_id):
    """
        Текущий заказ помечается как завершенный неоконченный.

    :param order_id:
    :param context:
    :type update: object
    """
    chat_id = update.message.chat_id
    update.message.reply_text(
        'Передумали? Возвращаем в начало...',
        reply_markup=hub_keyboard(chat_id))

    return ConversationHandler.END


def take_comment(update, context):
    """
        Отвечает на колбек юзера для прием комментария
    :type context: object
    :type update: object
    
    """
    print("Запрос комментария для заказа от мастера")
    message_text = update.message.text
    query = update.callback_query
    chat_id = query.message.chat_id
    message_id = update.callback_query.message.message_id
    data = re.findall(r'[\d]{1,15}', query.data)
    order_id, work_id = data[0], data[1]
    context.bot.delete_message(chat_id, message_id)
    save_comments_in_work_db(order_id=order_id,
                             chat_id=chat_id,
                             message_id=message_id,
                             message_text=message_text,
                             )
    buttons = [
        "Ожидает запчасть",
        "На диагностике",
        "Отправлен в Москву",
    ]
    reply_keyboard = questions_kbd(buttons)
    send_message_callback(update,
                          context,
                          message=f"Напишите рабочий статус у заказа #{order_id}",
                          reply=reply_keyboard)

    return TAKE_COMMENT


def save_comment(update, context):
    """
        Сохраняет меседж юзера
    :param update:
    :param context:
    :return:
    """
    print("Сохраняем комментарий")
    chat_id = update.message.chat_id
    order_id, message_id, message_text = get_order_id_on_commenting(chat_id=chat_id)
    kbd = hub_keyboard(chat_id)
    comment = update.message.text
    save_comments_in_work_db(comment=comment, order_id=order_id)
    send_message(update,
                 context,
                 message="Рабочий статус присвоен.",
                 reply=kbd,
                 )
    # сюда бы еще перепарсенный меседж с изменным статусом (новый меседж добавлением нужной строки)
    # ща поправлю инлайн команду

    return ConversationHandler.END


save_users_status_in_work_handler = ConversationHandler(
    entry_points=[CallbackQueryHandler(take_comment, entry_point_pattern)],

    states={
        TAKE_COMMENT: [MessageHandler(Filters.text, save_comment)],
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=False,
)
