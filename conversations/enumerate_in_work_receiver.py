#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime

from telegram.ext import CommandHandler
from telegram.ext import ConversationHandler
from telegram.ext import MessageHandler
from telegram.ext import Filters
from telegram import ParseMode

from rights.checking import check_rights

from keyboards.TG_keyboards import questions_kbd
from keyboards.TG_keyboards import hub_keyboard
from keyboards.TG_keyboards import inline_kbd_orders_in_work

from db_manager import get_order_in_orders_by_order_id
from db_manager import get_orders_in_work_by_id
from db_manager import get_orders_in_work_list_by_order_id
from db_manager import get_orders_in_work_list
from db_manager import get_clients_masters_id
from db_manager import get_client_id
from db_manager import get_orders_in_work_list_by_status
from db_manager import get_orders_in_work_list_by_order_id
from db_manager import get_users_status_in_work_list_by_order_id
from utils.parsers import calculate_geo_index

from utils.send_message import send_message_inline

from loguru import logger

# conv_handler constants
entry_phrase = "Заказы в работе"
# entry points
ENTRY_POINT = Filters.regex(f"^{entry_phrase}$")

# states
AWAIT_SELECTED_ROLE = 'selecting_role'
AWAIT_SELECT_TYPE = 'selecting_type'
SELECTED_NUMBER_ORDER = 'selecting_number_order'


def cancel(update, context, order_id):
    """
    команда отмены, обязательный аотруб конверсейшн хэндлера
    :param order_id:
    :param context:
    :type update: dict
    """
    chat_id = update.message.chat_id
    update.message.reply_text(
        'Отмена',
        reply_markup=hub_keyboard(chat_id))

    return ConversationHandler.END


def readable_data_from_timestamp(timestamp):
    return datetime.fromtimestamp(timestamp, ).strftime('%d.%m.%Y {} %H:%M').format("в")


def fun_entry(update, context):
    message = "По номеру или весь список?"

    buttons = [
        "Весь список",
        "По номеру",
        "Отмена",
    ]

    reply_keyboard = questions_kbd(buttons)

    send_message_inline(update, context, message, reply=reply_keyboard)

    return AWAIT_SELECT_TYPE


def fun_select_type(update, context):
    # собираем данные
    chat_id = update.message.chat_id
    users_text = update.message.text.lower()

    # ветвление
    if users_text == "по номеру":
        message = "Отправьте номер заказа"
        send_message_inline(update, context, message)
        return SELECTED_NUMBER_ORDER

    elif users_text == "весь список":
        enlist_orders_in_work_receiver(update, context)
        return ConversationHandler.END

    elif users_text == "отмена":
        reply_keyboard = hub_keyboard(chat_id)
        message = "Отменено"
        send_message_inline(update, context, message=message, reply=reply_keyboard)
        return ConversationHandler.END

    else:
        message = "Выберите один из указанных вариантов!"
        send_message_inline(update, context, message)
        return AWAIT_SELECT_TYPE


def fun_select_order(update, context):
    # константы
    chat_id = update.message.chat_id
    users_text = update.message.text.lower()
    reply_keyboard = hub_keyboard(chat_id)

    # отмена
    if users_text == "отмена":
        reply_keyboard = hub_keyboard(chat_id)
        message = "Отменено"
        send_message_inline(update, context, message=message, reply=reply_keyboard)
        return ConversationHandler.END

    # конвертим в инт
    try:
        users_text = int(users_text)
    except ValueError:
        message = "Нужны цифры, не буквы"
        send_message_inline(update, context, message)
        return SELECTED_NUMBER_ORDER

    # берем дату по заказу
    row, columns = get_order_in_orders_by_order_id(order_id=users_text)
    try:
        current_row = dict(zip(columns, row))
    except TypeError:
        message = "Такого заказа нет."
        send_message_inline(update, context, message=message, reply=reply_keyboard)
        return ConversationHandler.END

    # статус чек
    available_statuses = [
        "end_ok",
        "in_work",
    ]

    if current_row["status"] not in available_statuses:
        send_message_inline(update, context, message=f"Этот заказ завершён. \n "
                                                     f"{current_row['status']}", reply=reply_keyboard)
        return ConversationHandler.END

    # если ок
    # формируем соощение с готовым запросом
    message = order_message(current_row)
    # шлем месагу
    send_message_inline(update, context, message=message, parse_mode=ParseMode.HTML)

    # шлем месагу с клавиатурой реплуй
    message = "Поиск завершен."
    send_message_inline(update, context, message=message, reply=reply_keyboard)

    return ConversationHandler.END


def order_message(row):
    add_data = readable_data_from_timestamp(float(row['timestamp']))

    message = f'<b>№ заказа</b>: {row["order_id"]} \n' \
              f'Добавлен: {add_data} \n' \
              f'<b>Статус</b>: {row["status"]} \n' \
              f' \n' \
              f'<b><i>Информация об аппарате</i></b>\n' \
              f'<i>Производитель</i>: {row["brand_name"]} \n' \
              f'<i>Модель</i>: {row["model_name"]} \n' \
              f'<i>Целевая проблема</i>: {row["trouble"]} \n' \
              f'<i>Комментарий при приеме</i>: {row["comments_receive"]} \n' \
              f'<i>Цена работы</i>: {row["price"]} \n' \
              f'<i>Предоплата</i>: {row["prepayment"]} \n' \
              f' \n' \
              f'<b><i>Информация о клиенте</i></b>\n' \
              f'<i>Имя клиента</i>: {row["client_name"]} \n' \
              f'<i>Номер телефона клиента</i>: \n {row["phone_number"]} \n' \
              f'\n' \
        # f'<i>Исполнитель</i>: \n {master_id}'

    return message


def enlist_orders_in_work_receiver(update, context):
    """
        Выводит список заказов в работе, принадлежащих этому мастеру
    :return:
    """

    chat_id = update.message.chat_id
    print(chat_id)
    client_id = get_client_id(chat_id=chat_id)
    print(client_id)
    if check_rights(chat_id):
        # получаем строки с таблицы в работе и парсим
        work_rows, work_columns = get_orders_in_work_list_by_status(status='in_work')
        try:
            length = len(work_rows)
        except TypeError:
            length = 0
        if length == 0:
            send_message_inline(update, context, message="Нет выполняемых заказов.")

        else:
            # получаем данные по заказам
            orders_rows_in_work, columns = get_orders_in_work_list_by_status(status='in_work')
            print('orders_rows_in_work', orders_rows_in_work)
            print('columns', columns)

            dict_orders = {}
            for row in orders_rows_in_work:
                dict_orders[row[0]] = dict(zip(columns, row))

            print(dict_orders)

            # берем элементы словаря и распарсиваем в отправляемом сообщении
            for key in dict_orders:
                # адаптируем под человека дату создания заказа
                current_row = dict_orders[key]
                add_data = readable_data_from_timestamp(float(current_row['timestamp']))

                # проверяем юзерс статус
                users_status = get_users_status_in_work_list_by_order_id(order_id=current_row["order_id"])
                if users_status is None:
                    users_status = "Не указан"
                created_geo = calculate_geo_index(client_id=row["client_id"])
                # составляем меседж
                message = f'<b>№ заказа</b>: {current_row["order_id"]}{created_geo} \n' \
                          f'Добавлен: {add_data} \n' \
                          f'<b>Статус</b>: {current_row["status"]} \n' \
                          f'<b>Состояние заказа</b>: {users_status} \n' \
                          f' \n' \
                          f'<b><i>Информация об аппарате</i></b>\n' \
                          f'<i>Производитель</i>: {current_row["brand_name"]} \n' \
                          f'<i>Модель</i>: {current_row["model_name"]} \n' \
                          f'<i>Целевая проблема</i>: {current_row["trouble"]} \n' \
                          f'<i>Комментарий при приеме</i>: {current_row["comments_receive"]} \n' \
                          f'<i>Цена работы</i>: {current_row["price"]} \n' \
                          f'<i>Предоплата</i>: {current_row["prepayment"]} \n' \
                          f' \n' \
                          f'<b><i>Информация о клиенте</i></b>\n' \
                          f'<i>Имя клиента</i>: {current_row["client_name"]} \n' \
                          f'<i>Номер телефона клиента</i>: \n {current_row["phone_number"]} \n' \
                          f'\n' \
                          f'<i>Исполнитель</i>: \n {current_row["client_id"]}'

                send_message_inline(update, context, message=message, parse_mode=ParseMode.HTML)


get_in_work_orders_receiver_handler = ConversationHandler(
    entry_points=[MessageHandler(ENTRY_POINT, fun_entry)],

    states={
        AWAIT_SELECT_TYPE: [MessageHandler(Filters.text, fun_select_type)],
        SELECTED_NUMBER_ORDER: [MessageHandler(Filters.text, fun_select_order)],
    },
    fallbacks=[CommandHandler('cancel', cancel)], allow_reentry=False
)
