###########################################
# ╔══╗─╔══╗╔════╗
# ║╔╗║─║╔╗║╚═╗╔═╝
# ║╚╝╚╗║║║║──║║
# ║╔═╗║║║║║──║║
# ║╚═╝║║╚╝║──║║
# ╚═══╝╚══╝──╚╝
# ╔══╗╔═══╗╔════╗╔════╗╔══╗╔╗─╔╗╔═══╗╔══╗
# ║╔═╝║╔══╝╚═╗╔═╝╚═╗╔═╝╚╗╔╝║╚═╝║║╔══╝║╔═╝
# ║╚═╗║╚══╗──║║────║║───║║─║╔╗─║║║╔═╗║╚═╗
# ╚═╗║║╔══╝──║║────║║───║║─║║╚╗║║║╚╗║╚═╗║
# ╔═╝║║╚══╗──║║────║║──╔╝╚╗║║─║║║╚═╝║╔═╝║
# ╚══╝╚═══╝──╚╝────╚╝──╚══╝╚╝─╚╝╚═══╝╚══╝
# ╔╗
# ╚╝
# ╔╗
# ╚╝
##########################################
# ¶¶``¶¶``¶¶``¶¶
# ¶¶``¶¶``¶¶`¶¶
# ¶¶``¶¶``¶¶¶¶
# `¶¶¶¶```¶¶`¶¶
# ``¶¶````¶¶``¶¶

VK_TOKEN = "98ab8cc0ef27264397eeb974d3be217376027f59010ff85800a713e7d336267dc759608347506028f4ddb"
VK_GROUP_ID = '155703281'  # Id Group
VK_APP_ID = '7475610'  # Id app
VK_LOGIN = '89167835999'  # login
VK_PASSWORD = 'xSmtw$s92xTRLAN'  # pass_user

# list with admins
VK_TEST_USER_ID = [453450481,
                   ]  # Id user

ADMIN_CHAT_ID = "1085323286" # 1085323286-bronnicy
ADMIN_CHAT_ID_anton = "223781831"
###########################################
# ╔════╗╔═══╗╔╗──╔═══╗╔═══╗╔═══╗╔══╗╔╗──╔╗
# ╚═╗╔═╝║╔══╝║║──║╔══╝║╔══╝║╔═╗║║╔╗║║║──║║
# ──║║──║╚══╗║║──║╚══╗║║╔═╗║╚═╝║║╚╝║║╚╗╔╝║
# ──║║──║╔══╝║║──║╔══╝║║╚╗║║╔╗╔╝║╔╗║║╔╗╔╗║
# ──║║──║╚══╗║╚═╗║╚══╗║╚═╝║║║║║─║║║║║║╚╝║║
# ──╚╝──╚═══╝╚══╝╚═══╝╚═══╝╚╝╚╝─╚╝╚╝╚╝──╚╝

TG_BOT_TOKEN = "1674768111:AAFLA9nVeM_yZO3G-Q0VhswB0pjd95mPuMw"

# list with admins
TG_TEST_USER_ID = [823170382,
                   223781831,
                   ]

###########################################
# ╔════╗╔═══╗╔══╗╔════╗
# ╚═╗╔═╝║╔══╝║╔═╝╚═╗╔═╝
# ──║║──║╚══╗║╚═╗──║║
# ──║║──║╔══╝╚═╗║──║║
# ──║║──║╚══╗╔═╝║──║║
# ──╚╝──╚═══╝╚══╝──╚╝
TEST_MSG = 'Переподключение'  # test message when starts

###########################################
# ╔══╗─╔══╗
# ║╔╗╚╗║╔╗║
# ║║╚╗║║╚╝╚╗
# ║║─║║║╔═╗║
# ║╚═╝║║╚═╝║
# ╚═══╝╚═══╝
# ╔══╗╔═══╗╔════╗╔════╗╔══╗╔╗─╔╗╔══╗
# ║╔═╝║╔══╝╚═╗╔═╝╚═╗╔═╝╚╗╔╝║╚═╝║║╔═╝
# ║╚═╗║╚══╗──║║────║║───║║─║╔╗─║║╚═╗
# ╚═╗║║╔══╝──║║────║║───║║─║║╚╗║╚═╗║
# ╔═╝║║╚══╗──║║────║║──╔╝╚╗║║─║║╔═╝║
# ╚══╝╚═══╝──╚╝────╚╝──╚══╝╚╝─╚╝╚══╝
DB_NAME = 'main_database'

# таблицы расписаны для совместимости для уже написанного кода

# table with clients
DATABASE_TABLE_CLIENTS_NAME = 'clients'
# :param client_id: inner user id
# :param chat_id: telegram user_id
# :param name: telegram User.name
# :param timestamp: Unix timestamp float
DATABASE_TABLE_CLIENTS_FIELDS = [
    'client_id',
    'chat_id',
    'name',
    'current_state',
    'timestamp',
]

# table with phone numbers
DATABASE_TABLE_CLIENTS_NUMBERS_NAME = 'numbers'
DATABASE_TABLE_CLIENTS_NUMBERS_FIELDS = [
    'number_id',
    'client_id',
    'number',
    'timestamp',
]

# table with inherited comments about orders
DATABASE_CALL_CENTER_NAME = 'calls'
DATABASE_CALL_CENTER_FIELDS = [
    'result_id',
    'order_id',
    'result_string',
    'timestamp',
]

# table with orders
DATABASE_TABLE_ORDERS_NAME = 'orders'
DATABASE_TABLE_ORDERS_FIELDS = [
    'order_id',
    'phone_number',
    'model_name',
    'condition',
    'trouble',
    'pic_path',
    'pic_id',
    'oriented_datetime',
    'status',
    'timestamp',
]

# table with addresses
DATABASE_TABLE_ADDRESSES_NAME = 'addresses'
DATABASE_TABLE_ADDRESSES_FIELDS = [
    'address_id',
    'order_id',
    'address_text',
]
# table with details
DATABASE_TABLE_DETAILS_NAME = 'details'
DATABASE_TABLE_DETAILS_FIELDS = [
    'detail_id',
    'order_id',
    'detail_text',
    'status',
    'timestamp',
]

# table with workpool
DATABASE_TABLE_WORK_POOL_NAME = 'in_work'
DATABASE_TABLE_WORK_POOL_FIELDS = [
    'work_id',
    'order_id',
    'client_id',
    'status',
    'timestamp',
]

# table with delivery pool
DATABASE_TABLE_DELIVERY_POOL_NAME = 'on_delivery'
DATABASE_TABLE_DELIVERY_POOL_FIELDS = [
    'delivery_id',
    'order_id',
    'client_id',
    'status',
    'timestamp',
]

# table with delivery pool
DATABASE_TABLE_DELIVERED_POOL_NAME = 'delivered'
DATABASE_TABLE_DELIVERED_POOL_FIELDS = [
    'finished_id',
    'order_id',
    'client_id',
    'timestamp',
]


# система ролей и прав
#
DATABASE_TABLE_RIGHTS_NAME = 'rights'
DATABASE_TABLE_RIGHTS_FIELDS = [
    'right_id',
    'right_name',

]

DATABASE_TABLE_ROLES_NAME = 'roles'
DATABASE_TABLE_ROLES_FIELDS = [
    'role_id',
    'role_name',
    'rights',
]
#
DATABASE_TABLE_STAFF_NAME = 'staff'
DATABASE_TABLE_STAFF_FIELDS = [
    'user_id',
    'right_name',
]

# LIST TABLES
db_tables_ = {DATABASE_TABLE_CLIENTS_NAME: DATABASE_TABLE_CLIENTS_FIELDS,
              DATABASE_TABLE_CLIENTS_NUMBERS_NAME: DATABASE_TABLE_CLIENTS_NUMBERS_FIELDS,
              DATABASE_TABLE_DETAILS_NAME: DATABASE_TABLE_DETAILS_FIELDS,
              DATABASE_CALL_CENTER_NAME: DATABASE_CALL_CENTER_FIELDS,
              DATABASE_TABLE_ORDERS_NAME: DATABASE_TABLE_ORDERS_FIELDS,
              DATABASE_TABLE_ADDRESSES_NAME: DATABASE_TABLE_ADDRESSES_FIELDS,
              DATABASE_TABLE_ROLES_NAME: DATABASE_TABLE_ROLES_FIELDS,
              DATABASE_TABLE_RIGHTS_NAME: DATABASE_TABLE_RIGHTS_FIELDS,
              DATABASE_TABLE_STAFF_NAME: DATABASE_TABLE_STAFF_FIELDS,
              }

# шортлист id для Pool
db_tables = {DATABASE_TABLE_CLIENTS_FIELDS[0]: DATABASE_TABLE_CLIENTS_NAME,
             DATABASE_TABLE_CLIENTS_NUMBERS_FIELDS[0]: DATABASE_TABLE_CLIENTS_NUMBERS_NAME,
             DATABASE_CALL_CENTER_FIELDS[0]: DATABASE_CALL_CENTER_NAME,
             DATABASE_TABLE_ORDERS_FIELDS[0]: DATABASE_TABLE_ORDERS_NAME,
             DATABASE_TABLE_CLIENTS_NUMBERS_FIELDS[0]: DATABASE_TABLE_CLIENTS_NUMBERS_NAME,

             }

###########################################
# ╔═══╗╔╗╔╗╔═══╗╔══╗╔════╗╔══╗╔══╗╔╗─╔╗╔══╗
# ║╔═╗║║║║║║╔══╝║╔═╝╚═╗╔═╝╚╗╔╝║╔╗║║╚═╝║║╔═╝
# ║║─║║║║║║║╚══╗║╚═╗──║║───║║─║║║║║╔╗─║║╚═╗
# ║║╔╝║║║║║║╔══╝╚═╗║──║║───║║─║║║║║║╚╗║╚═╗║
# ║╚╝─║║╚╝║║╚══╗╔═╝║──║║──╔╝╚╗║╚╝║║║─║║╔═╝║
# ╚═══╝╚══╝╚═══╝╚══╝──╚╝──╚══╝╚══╝╚╝─╚╝╚══╝

# TODO: сделать кнопки на момент сьемки:
# "Сделать фото" (вызов камеры клиента), "выбрать фото", "Отказ" с соотвествующим колбеком
QUESTIONS = {"name": "Как к вам обращаться.",
             "phone_number": "Номер, по которому которому можем позвонить и уточнить информацию",
             "model_name": "Наименование или обозначение устройства. Нажмите на кнопки ниже.\n "
                           "Или напишите самостостоятельно, если его нет на кнопках.",
             "condition": "Опишите внешнее состояние, нажимая на кнопки:",
             "target_problem": "Проблема, которую требуется решить 👇",
             "pic_id": "Сделайте фото для ускоренного анализа стоимости ремонта.",
             }

###########################################
# Brands
BRANDS = ['Apple',
          'Samsung',
          'Huawei',
          'Honor',
          'Oppo',
          'Xiaomi',
          'Redmi',
          'ZTE',
          'Nokia',
          'Sony',
          'Meizu',
          'OnePlus',
          ]

##############################################
CONDITIONS = ['Сломан дисплей',
              'Царапины',
              'Поношенный',
              'Новый',
              ]
