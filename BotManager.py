#!/usr/bin/env python
# -*- coding: utf-8 -*-


# other packages
import logging
# built in
import sqlite3
from time import time

from loguru import logger
from telegram import ParseMode
from telegram.ext import CallbackQueryHandler
from telegram.ext import CommandHandler
from telegram.ext import Filters
from telegram.ext import MessageHandler
from telegram.ext import Updater

from Pool import Pool
from callbacks.back_to_receive_pool import change_back_received_pool_orders_handler
from callbacks.client_ask_explaining_callback import ask_explaining
from callbacks.move_new_order_to_delivery_callback import move_new_order_to_delivery_callback
from callbacks.price_query_callback import make_query
# project classes
from callbacks.search_by_order_id import full_search_order_id_callback
from callbacks.take_payment_detail import take_payment_detail
# callbacks
from callbacks.take_payments import get_payments_on_deliver_handler
from callbacks.take_users_status_in_work_order import save_users_selected_fun_in_work_fun
from callbacks.ticket_send_model import ask_model
from callbacks.ticket_send_photo_callback import ask_photo
from callbacks.to_delivered_pool import move_delivered_pool_orders_handler
from callbacks.to_delivery_pool import move_delivery_pool_orders_handler, ask_for_price_to_delivery
from conversations.ChangeRole import change_role_handler
# conversations
from conversations.GetUsersNumber import number_receive_handler
from conversations.NewOrder import order_receive_handler
from conversations.NewOrderReceiver import order_receive_handler_receiver
from conversations.ShowNewOrders import get_new_orders_handler, check_rights
from conversations.TestForGlobalVarsConversation import test_globals_handler
from conversations.callback_move_to_work_pool import change_work_pool_orders_handler
from conversations.enumerate_details_orders import get_list_details_orders
from conversations.enumerate_in_work_receiver import get_in_work_orders_receiver_handler
from conversations.in_work_client import get_in_work_orders_handler
from conversations.price_repair_client import get_price_client_handler
from conversations.enumerate_on_delivery_receiver import get_orders_on_deliver_handler, order_message
from conversations.search_orders_receiver import search_all_orders_receiver_handler
from conversations.enumerate_new_orders_receiver import get_new_orders_receiver_handler
from db_manager import check_existing_user, get_ticket_chat_id, get_order_status, change_status_order, get_order_data, \
    change_status_work, insert_delivery_pool, get_client_id, \
    get_order_data_delivery_col, get_order_data_col
# project functions
from db_manager import create_tables
from db_manager import get_selected_function
from db_manager import save_clients_function_db
from db_manager import save_orders_users_status_db
from hello import Hello
from keyboards.TG_keyboards import hub_keyboard, inline_kbd_ticket_for_client_send_photo, inline_kbd_ticket, \
    inline_kbd_ticket_for_client_send_text, inline_kbd_ticket_for_client_send_model, \
    inline_kbd_ticket_for_client_send_answer, inline_kbd_orders_on_delivery
from utils.media_pool import photos_send_question_admin
# keyboards
from keyboards.TG_keyboards import questions_kbd  # rename to reply_layout_helper
from settings import DATABASE_TABLE_CLIENTS_NAME as CLIENTS_TBL_NAME
# settings
from settings import DB_NAME as db
from settings import db_tables_
from settings import ADMIN_CHAT_ID
from utils.send_message import send_message_inline


def check_base():
    """
        self test
        """
    try:
        create_tables(db, db_tables_)
    except sqlite3.OperationalError as e:
        print(f'{e}')

    return None


def send_message_to_target(context, chat_id=None, text=None, reply=None):
    """Отправляет сообщение для конкретного chat_id
    :type context: Объект контекста
    :type chat_id: целевой chat_id
    :type text: строка с текстом
    :param reply: inline клавиатура
    """
    context.bot.send_message(chat_id=chat_id, text=text, reply_markup=reply)

    return None


def send_message(update=None, context=None, message=None, reply=None):
    """context based send message for any reason"""
    context.bot.send_message(
        chat_id=update.message.chat_id,
        text=message,
        reply_markup=reply,
    )

    return None


def contacts_kbd():
    get_user_defined_contact = "Поделится номером"
    get_new_order = "Новый заказ"

    buttons = [get_user_defined_contact,
               get_new_order
               ]
    return questions_kbd(buttons)


def start_keyboard():
    buttons = ["Новый заказ",
               "Мои заказы",
               ]

    return questions_kbd(buttons)


def echo(update, context):
    """
        echo tree any context
    :param update:
    :param context:
    :return: None
    """

    chat_id = update.message.chat.id
    user_name = update.message.chat.username
    text = update.message.text

    # TODO: переместить в хаб
    function, param = get_selected_function(chat_id=chat_id)
    print(function, param)

    # Общая ветка отмены.
    try:
        if text.lower() == 'отмена':
            save_clients_function_db(chat_id=chat_id, selected_function='', params='')
            send_message(update, context, message="Отменено")
    except AttributeError:
        pass

    # Ответвление функций
    if function is None:
        print('chat_id:', chat_id, 'user_name:', user_name, 'text:', text)
        send_message(update, context, "Выберите режим", reply=hub_keyboard(chat_id))

    elif function[:-1] == 'take_comments':
        """функция комментариев в 1 проход, что вызывает сброс поля function None"""
        save_orders_users_status_db(order_id=param, users_status=text)
        send_message(update, context, "Статус заказа изменен", reply=hub_keyboard(chat_id))
        save_clients_function_db(chat_id=chat_id, selected_function='', params='')

    elif function == 'price_on_delivery':
        "Перехват меседжа от юзера при выдаче "
        move_delivery_pool_orders_handler(update, context)

    elif function == 'price_query':
        """ От админа ->
            Парсинг меседжей от юзера, если он админ и отвечает по тикету"""
        # send_messages with special functions

        # получение целевого чат_ид = по параметру функции определяем тикет и выдергиваем чат ид тикета
        target_chat_id = get_ticket_chat_id(ticket_id=param)

        if "Отправьте фото" in text:
            """Суппорт отправляет задачу"""
            # инлайн клавиатура для отправки фото
            reply = inline_kbd_ticket_for_client_send_photo(ticket_id=param)
            # отправка сообщения
            send_message_to_target(context, chat_id=target_chat_id, text=text, reply=reply)

        elif "Опишите неисправность" in text:
            # инлайн клавиатура для отправки фото
            reply = inline_kbd_ticket_for_client_send_text(ticket_id=param)
            # отправка сообщения
            send_message_to_target(context, chat_id=target_chat_id, text=text, reply=reply)

        elif "Укажите точную модель" in text:
            # присобачить пул доступных документов (загруженных ботом и имеющим файл_ид)
            photos_send_question_admin(context, chat_id=target_chat_id)
            # здесь клавиатура отдельно чтобы пользователь мог отослать как фото, так и текст
            reply = inline_kbd_ticket_for_client_send_model(ticket_id=param)
            text = "Отправьте точную модель устройства. Она указана на задней крышке, в настройках или на коробке"
            send_message_to_target(context, chat_id=target_chat_id, text=text, reply=reply)

        else:
            reply = inline_kbd_ticket_for_client_send_answer(ticket_id=param)
            send_message_to_target(context, chat_id=target_chat_id, text=text, reply=reply)

        # сбрасываем состояние админа
        save_clients_function_db(chat_id=chat_id, selected_function='', params='')
        # конец ветки парсера

    elif function == 'ticket send photo':
        """От клиента ->
            перехватывает фото для заданного тикета, 
                в случае текста пренаправляет его отправить еще раз """
        fail_message = "Не видно фото. Попробуй еще раз!"
        try:
            message_photo = update.message.photo[0]['file_id']
        except IndexError or AttributeError:  # почему-то оператор "или" не работает
            print(update.message)
            send_message(update, context, message=fail_message, reply=hub_keyboard(update.message.chat_id))
            pass
        else:
            if text is not None:
                send_message(update, context, message=text)
                send_message(update, context,
                             message="Сообщение отправлено, ожидайте ответа",
                             reply=hub_keyboard(update.message.chat_id))
            else:
                # отправляет админу фото
                context.bot.send_photo(chat_id=ADMIN_CHAT_ID,
                                       photo=message_photo,
                                       reply_markup=inline_kbd_ticket(ticket_id=param))
                # Ответ пользователю об успехе
                send_message(update, context,
                             message="Фото отправлено, ожидайте ответа",
                             reply=hub_keyboard(update.message.chat_id))

            save_clients_function_db(chat_id=chat_id, selected_function='', params='')

    elif function == 'ticket send explain client':
        """От клиента ->
            перехватывает сообщение для заданного тикета,
                в случает отсуствия текста - перенаправляет отправить его еще раз
        """
        fail_message = "Хмм, вы отправили что-то другое. Попробуйте еще раз."
        if text != "":
            # отправляет сообщение админу
            context.bot.send_message(chat_id=ADMIN_CHAT_ID,
                                     text=text,
                                     reply_markup=inline_kbd_ticket(ticket_id=param))
            # Ответ пользователю об успехе
            send_message(update, context, message="Фото отправлено, ожидайте ответа")
            save_clients_function_db(chat_id=chat_id, selected_function='', params='')
        else:
            send_message(update, context, message=fail_message)

    elif function == "ticket send model":
        """От клиента ->
                    перехватывает сообщение для заданного тикета. Для фото и текста
        """
        try:
            message_photo = update.message.photo[0]['file_id']
        except IndexError:
            print(update.message)
            pass
            # нет фото, но текст присуствует
            if text is not None:
                send_message_to_target(context, chat_id=ADMIN_CHAT_ID,
                                       text=text, reply=inline_kbd_ticket(ticket_id=param))
        # фото есть
        else:
            context.bot.send_photo(chat_id=ADMIN_CHAT_ID,
                                   photo=message_photo,
                                   reply_markup=inline_kbd_ticket(ticket_id=param))
            # Ответ пользователю об успехе
            send_message(update, context, message="Фото отправлено, ожидайте ответа")
            save_clients_function_db(chat_id=chat_id, selected_function='', params='')

    else:
        # anyone
        print('chat_id:', chat_id, 'user_name:', user_name, 'text:', text)
        print('context:', dir(context))
        send_message(update, context, "Вы потерялись? Возвращаем в начало...", reply=hub_keyboard(chat_id))

    return None


class BotManager:

    def __init__(self, token):
        self.start_handler = CommandHandler('start', self.handler_start, pass_args=True, )
        self.pool = Pool(db)
        self.token = token
        self.dispatcher = None
        self.updater = Updater
        self.logger = logging.getLogger(__name__)

    def create_order(self):
        pass

    def handler_start(self, update, context):
        """
            /start
            """
        print("start command called")
        print("callback context", context.args)

        if not check_existing_user(db, CLIENTS_TBL_NAME, update.message.chat_id):
            client_id = self.pool.retrieve_last_id('client_id')
            hello = Hello(update, context, client_id)
            hello.start()

        elif context.args is not None:

            # проверить статус заказа и в зависимости от него свормировать инлайн кнопки
            # хотя если я ввожу переход по ссылке с аргументом, то я просто перевожу его в выдачу и
            # показывю меседж с инлайном на выдачу
            client_id = get_client_id(chat_id=update.message.chat_id)

            args = context.args[0].split("-")
            if args[0] == "order":
                print("argue called")
                if check_rights(update.message.chat_id):

                    try:
                        order_id = int(args[1])
                    except TypeError:
                        send_message(update, context, message="Ошибка аргумента.")

                    else:
                        status = get_order_status(order_id=order_id)
                        if status == "end_ok" or "on_delivery":
                            # меняет в таблице работы статус на выполненный
                            change_status_work(order_id=order_id,
                                               status='on_delivery',
                                               )

                            # меняет статус заказа на on_delivery
                            change_status_order(order_id=order_id,
                                                status='on_delivery',
                                                )

                            # добавляет в таблицу с выдачей строку
                            insert_delivery_pool(order_id=order_id,
                                                 client_id=client_id,
                                                 status='on_delivery',
                                                 timestamp=time(),
                                                 )
                            # добавляем инлайн клавиатуру под сообщение с заказом
                            delivery_row = get_order_data_delivery_col(order_id=order_id)
                            delivery_row = dict(zip(delivery_row[1], delivery_row[0]))
                            print(delivery_row)

                            inline_kbd = inline_kbd_orders_on_delivery(delivery_row['order_id'],
                                                                       delivery_row['delivery_id'])
                            order_row = get_order_data_col(order_id=order_id)
                            order_row = dict(zip(order_row[1], order_row[0]))
                            message = order_message(order_row)
                            send_message_inline(update, context, message=message, reply=inline_kbd,
                                                parse_mode=ParseMode.HTML)

                        else:
                            send_message(update, context,
                                         message=f"Заказ имеет статус {status}. Проверьте его, прежде чем выдавать")

        else:
            # хаб
            # проверим юзера по таблицам - есть ли он в таблице staff,

            # затем по роли выдать ему клавиатуру И нужные конверсейшны

            send_message(update,
                         context,
                         "Добро пожаловать в начало.\n "
                         "Можете добавить еще номер",
                         reply=hub_keyboard(update.message.chat_id))

        return None

    def receive_contact(self):
        pass

    @logger.catch
    def load_handlers(self):
        # start
        self.dispatcher.add_handler(self.start_handler)

        # conversation to receive nums
        self.dispatcher.add_handler(number_receive_handler)
        # conversation to receive orders (common/clients)
        self.dispatcher.add_handler(order_receive_handler)
        self.dispatcher.add_handler(get_price_client_handler)
        # conversation to receive orders from receiver
        self.dispatcher.add_handler(order_receive_handler_receiver)
        self.dispatcher.add_handler(change_role_handler)
        self.dispatcher.add_handler(get_new_orders_handler)
        self.dispatcher.add_handler(get_new_orders_receiver_handler)
        self.dispatcher.add_handler(get_in_work_orders_handler)
        self.dispatcher.add_handler(get_orders_on_deliver_handler)
        self.dispatcher.add_handler(get_in_work_orders_receiver_handler)
        self.dispatcher.add_handler(test_globals_handler)
        self.dispatcher.add_handler(get_list_details_orders)
        self.dispatcher.add_handler(search_all_orders_receiver_handler)

        # callbacks
        self.dispatcher.add_handler(CallbackQueryHandler(change_work_pool_orders_handler,
                                                         pattern='^change order_id',
                                                         ),
                                    group=0,
                                    )
        self.dispatcher.add_handler(CallbackQueryHandler(change_back_received_pool_orders_handler,
                                                         pattern='^move back',
                                                         ),
                                    group=0,
                                    )
        self.dispatcher.add_handler(CallbackQueryHandler(ask_for_price_to_delivery,
                                                         pattern='^on delivery',
                                                         ),
                                    group=0,
                                    )
        self.dispatcher.add_handler(CallbackQueryHandler(move_delivered_pool_orders_handler,
                                                         pattern='^at client',
                                                         ),
                                    group=0,
                                    )

        self.dispatcher.add_handler(CallbackQueryHandler(save_users_selected_fun_in_work_fun,
                                                         pattern='^take_comments',
                                                         ),
                                    group=0,
                                    )

        self.dispatcher.add_handler(CallbackQueryHandler(take_payment_detail, pattern='^detail_at_client',
                                                         ),
                                    group=0,
                                    )
        self.dispatcher.add_handler(CallbackQueryHandler(move_new_order_to_delivery_callback,
                                                         pattern='^move_to_delivery',
                                                         ),
                                    group=0,
                                    )
        self.dispatcher.add_handler(CallbackQueryHandler(full_search_order_id_callback,
                                                         pattern='^full_search',
                                                         ),
                                    )
        self.dispatcher.add_handler(CallbackQueryHandler(make_query,
                                                         pattern='^ticket answer',
                                                         ),
                                    group=0,
                                    )
        self.dispatcher.add_handler(CallbackQueryHandler(ask_photo,
                                                         pattern='^ticket send photo client',
                                                         ),
                                    group=0,
                                    )

        self.dispatcher.add_handler(CallbackQueryHandler(ask_explaining,
                                                         pattern='^ticket send explain client',
                                                         ),
                                    group=0,
                                    )
        self.dispatcher.add_handler(CallbackQueryHandler(ask_model,
                                                         pattern='^ticket send model client',
                                                         ),
                                    group=0,
                                    )

        # self.dispatcher.add_handler(CallbackQueryHandler(колбек, pattern='^detail_at_client',
        #                                                ),
        #                          group=0,
        #                         )

        # 2nd variant (add MessageHandler with callback entry point -> overrides another same methods
        self.dispatcher.add_handler(get_payments_on_deliver_handler)
        # another handler for another callback
        # self.dispatcher.add_handler(save_users_status_in_work_handler)
        # echo must be last
        self.echo_handler = MessageHandler(Filters.all, echo)
        self.dispatcher.add_handler(self.echo_handler)

        return None

    @logger.catch
    def start_poll(self):
        """
        """
        # init db and tables
        check_base()
        # init Pool manager
        # init poll
        self.updater = Updater(token=self.token, use_context=True)
        self.dispatcher = self.updater.dispatcher

        # add handlers
        self.load_handlers()

        # start poll
        self.updater.start_polling()
        self.updater.idle()
