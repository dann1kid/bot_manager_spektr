#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re

from db_manager import get_client_geo


def parse_query(update):
    """

    :param update:
    :return:
    """
    query = update.callback_query

    # поиск идентификаторов
    data = re.findall(r'[\d]{1,15}', query.data)
    ticket_id = data[0]

    return ticket_id


def calculate_geo_index(client_id=None):
    """
    Возвращает индекс для заказов
    Args:
        client_id:

    Returns:
        created_geo

    """
    created_geo = get_client_geo(client_id=client_id)
    if created_geo == "bronnicy":
        created_geo = "Б"
    elif created_geo == "kolomna":
        created_geo = "К"
    else:
        created_geo = ""

    return created_geo
