#!/usr/bin/env python
# -*- coding: utf-8 -*-


def cancel(update, context, order_id):
    """
        Текущий заказ помечается как завершенный неоконченный.

    :param order_id:
    :param context:
    :type update: object
    """

    update.message.reply_text(
        'Передумали? Возвращаем в начало...',
        reply_markup=hub_keyboard(chat_id))

    client_id = get_client_id(db, table_clients, chat_id=update.message.chat_id)

    current_order = get_clients_current_order(db, table_orders, client_id, current_status)

    save_current_status_cancel(
        order_id=order_id,
        status=CURRENT_STATUS_UNSUCCESSFUL_END,
    )

    return ConversationHandler.END
