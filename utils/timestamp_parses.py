# !/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime


def readable_data_from_timestamp(timestamp):
    return datetime.fromtimestamp(timestamp, ).strftime('%d.%m.%Y {} %H:%M').format("в")
