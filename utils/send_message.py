# !/usr/bin/env python
# -*- coding: utf-8 -*-
from loguru import logger
import types
from telegram import ReplyKeyboardRemove


def send_message_inline(update, context, message, reply=None, parse_mode=None) -> None:
    """context based send message for any reason
    :param parse_mode:
    :type update: object
    :param context:
    :param message:
    :param reply:

    :return None
    """
    if reply == 'remove':
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            reply_markup=ReplyKeyboardRemove(),
            parse_mode=parse_mode,
        )
    elif reply is None:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            parse_mode=parse_mode,
        )
    else:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            reply_markup=reply,
            parse_mode=parse_mode,
        )

    return None


def send_message(update, context, message, reply=None) -> None:
    """context based send message for any reason
    :type update: object
    :param context:
    :param message:
    :param reply:

    :return None
    """
    if reply == 'remove':
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            reply_markup=ReplyKeyboardRemove())
    elif reply is None:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
        )
    else:
        context.bot.send_message(
            chat_id=update.message.chat_id,
            text=message,
            reply_markup=reply,
        )

    return None


def send_message_callback(update, context, message, reply=None, parse_mode=None) -> None:
    """context based send message for any reason
    :param update: 
    :param context:
    :param message:
    :param reply:
    :param parse_mode:
    :return None
    """
    if reply == 'remove':
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=message,
            reply_markup=ReplyKeyboardRemove(),
            parse_mode=parse_mode,
        )
    elif reply is None:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=message,
            parse_mode=parse_mode,
        )
    else:
        context.bot.send_message(
            chat_id=update.effective_chat.id,
            text=message,
            reply_markup=reply,
            parse_mode=parse_mode,
        )

    return None
