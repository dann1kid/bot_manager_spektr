# !/usr/bin/env python
# -*- coding: utf-8 -*-
from db_manager import check_in_staff
from db_manager import check_in_staff


def check_rights(chat_id):
    """ Простая проверка на право вызвать этот список
        TODO: заменить хардлист на список с бд
    """
    approved_roles = ['master',
                      'receiver',
                      'admin',

                      ]

    role = check_in_staff(chat_id=chat_id)

    if role in approved_roles:
        return True
    else:
        return False
