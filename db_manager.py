###############################################################################
# SQLite3
import sqlite3
from sqlite3 import OperationalError
from time import time

from settings import DATABASE_TABLE_WORK_POOL_NAME as work_table
from settings import DATABASE_TABLE_CLIENTS_NAME as clients_table
from settings import DATABASE_TABLE_ORDERS_FIELDS
from settings import DATABASE_TABLE_ORDERS_NAME as orders_table
from settings import DB_NAME as db
from settings import DATABASE_TABLE_DELIVERY_POOL_NAME as delivery_table
from settings import DATABASE_TABLE_DELIVERED_POOL_NAME as delivered_table

TICKETS_TABLE = "tickets"

table_payment_state = "payments_statements"


def insert_with(db, query):
    """Попытка сократить шаблонный код"""
    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)

    return None


def create_table_sqlite(db, table, columns):
    """если база существует - добавляет таблицу,
    если не существует создает базу данных
    и дописывает таблицу.
    
    Переделать под тип столбцов

    :param db: база данных
    :param table: таблица (название)
    :param columns: список столбцов
    :return bool:
    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()

        # declare string for columns
        str_columns = ''

        # unpack columns list into string
        for i in columns:
            str_columns += f'{i}, '

        # slice last comma and space then insert table
        print(f'create table {table} ( {str_columns[:-2]} )')
        cursor.execute(f'create table {table} ( {str_columns[:-2]} )')
        connector.close()

        return True


def create_hard_tables():
    pass


def create_tables(db, tables_list):
    """Создает из словаря таблицы в базе данных
        словарь существует в виде:
        tables = { table_name_0: [list_columns],
                   table_name_1: [list_columns],
                }
        """
    for key, value in tables_list.items():
        create_table_sqlite(db, key, value)

    return None


def save_order_sqlite(db,
                      table,
                      name,
                      phone_number,
                      model_name=None,
                      condition=None,
                      trouble=None,
                      pic_path=None,
                      pic_id=None,
                      oriented_datetime=None,
                      status=None,
                      ):
    """Функция сохраняющая заявки в базу sqlite3
    :param db: database to connect
    :param table: пул заказа.
    :param name: имя заказчика
    :param phone_number: номер заказчика
    :param condition: описание состояния аппарата от пользователя
    :param trouble: целевая проблема
    :param pic_path: string file path: путь к фото проблемного устройства
    :param pic_id: string: file_id telegram
    :param model_name: модель аппарата
    :param oriented_datetime: datetime string
    :param status: status order

    :return None:
    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f'INSERT INTO {table} '

        for i in DATABASE_TABLE_ORDERS_FIELDS:
            query += f' {i}, '

        print(query)
        cursor.execute(f"INSERT INTO {table} "
                       f"( name, "
                       f"phone_number, "
                       f"model_name, "
                       f"condition, "
                       f"trouble, "
                       f"pic_path, "
                       f"pic_id, "
                       f"oriented_datetime, "
                       f"status "
                       f") "
                       f"VALUES ("
                       f"'{name}', "
                       f"'{phone_number}', "
                       f"'{model_name}', "
                       f"'{condition}', "
                       f"'{trouble}', "
                       f"'{pic_path}', "
                       f"'{pic_id}', "
                       f"'{oriented_datetime}', "
                       f"'{status}' "
                       f")"
                       )

        print('rowid =', cursor.lastrowid)
        connector.commit()
        connector.close()

    return None


def get_last_id(db):
    """Получение последнего rowid.
    :param db: string: имя файла базы sqlite3
    :return last_id: последняя запись в бд
    """
    try:
        connector = sqlite3.connect(db)
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        cursor.execute(f"")
        connector.close()

    return None


def save_new_detail(db, table, num_order, detail):
    """Добавляет строку в таблицу
     для деталей в заказ.
     :param db: string: бд
     :param table: string: таблица куда сохранять
     :param num_order: int: номер заказа для детали.
     :param detail: string: деталь описание (например ссылка)
     :return None:
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        cursor.execute(f"INSERT INTO {table} ( num_order, detail ) "
                       f"VALUES ( '{num_order}',  '{detail}' ) ;")
        connector.commit()
        connector.close()

    return None


def check_in_staff(db=db, table=clients_table, chat_id=None) -> str:
    """
        Проверяет наличие юзера в таблице сотрудников и возвращает его роль.

    :param chat_id:
    :param table:
    :type db: object
    :return rights: string: текущая роль
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"SELECT role from {table} WHERE chat_id like {chat_id};"

        print(query)
        cursor.execute(query)
        role = cursor.fetchone()
        if role is None:
            return 'client'
        else:
            return role[0]


def save_name(db=db, table=orders_table, client_id=None, name=None, status=None):
    """ сохраняет имя клиента на приемке
    :param client_id:
    :type db: object
    :param name:
    :param table:
    :param status:
    """
    timestamp = time()
    name = str_translator(name)
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        name = str_translator(name)
        query = f" INSERT INTO {table} (client_id, status, timestamp, client_name ) " \
                f" VALUES ( '{client_id}','{status}', '{timestamp}', '{name}' ) ;"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_name_receiver(db=db, table=orders_table, name=None, status=None, order_id=None):
    """ сохраняет имя клиента на приемке
    :param order_id:
    :type db: object
    :param name:
    :param table:
    :param status:
    """
    name = str_translator(name)
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        name = str_translator(name)
        query = f" UPDATE {table} SET" \
                f" status='{status}', " \
                f" client_name='{name}'  " \
                f" WHERE order_id  LIKE '{order_id}';"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_phone_number_receiver(db=db, table=orders_table, order_id=None, client_id=None, phone_number=None,
                               status=None):
    """ сохраняет имя клиента на приемке
    :param order_id:
    :param client_id:
    :type db: object
    :param phone_number:
    :param table:
    :param status:
    """
    timestamp = time()
    # phone_number = str_translator(phone_number)
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"INSERT INTO {table} (order_id, client_id, phone_number, status, timestamp ) " \
                f"VALUES ('{order_id}', '{client_id}', '{phone_number}', '{status}', '{timestamp}' ) "
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_surname(db=db, table=orders_table, current_order=None, status=None, surname=None):
    """
        Сохраняет фамилию
    :param status:
    :param db:
    :param table:
    :param current_order:
    :type surname: object
    """
    surname = str_translator(str(surname))
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # insert yes/no in {delivery_status} value
        surname = str_translator(surname)
        query = f" UPDATE {table} SET status='{status}', client_surname='{surname}'" \
                f" WHERE order_id LIKE '{current_order}';"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_middle_name(db=db, table=orders_table, current_order=None, status=None, middle_name=None):
    """
        Сохраняет отчество
    :param status:
    :param middle_name:
    :param db:
    :param table:
    :param current_order:
    """
    middle_name = str_translator(middle_name)
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # insert yes/no in {delivery_status} value
        middle_name = str_translator(middle_name)
        query = f" UPDATE {table} SET client_middle_name='{middle_name}', status='{status}' " \
                f" WHERE order_id LIKE '{current_order}'; "
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def check_existing_user(db, table, chat_id):
    """Checking chat_id in database.
        :db str: db to connect 
        :table :
        :return bool:
        """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"SELECT client_id from {table} WHERE chat_id like {chat_id};"

        print(query)
        cursor.execute(query)
        if cursor.fetchone() is None:
            connector.close()
            return False
        else:
            connector.close()
            return True


def save_new_user(db, table, client_id, chat_id, name, current_state, timestamp):
    """Добавляет строку в таблицу
     для деталей в заказ.
     :param client_id:
     :param current_state:
     :param table: string: таблица куда сохранять
     :param db: string: бд

     :param chat_id:
     :param name:
     :param timestamp:
     :return None:
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"INSERT INTO {table} (client_id, chat_id, name, current_state, timestamp) " \
                f"VALUES ('{client_id}', '{chat_id}', '{name}', '{current_state}', '{timestamp}') "
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    return None


def get_client(db, table, chat_id):
    """
        Возвращает присутствие в таблице по chat_id
    :param chat_id: str
    :param db: str
    :param table: str
    :return object: client
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"SELECT * from {table} WHERE chat_id LIKE {chat_id};"

        print(query)
        cursor.execute(query)
        if cursor.fetchone() is None:
            connector.close()
            return False
        else:
            connector.close()
            return True


def get_order_data(db=db, table=orders_table, order_id=None):
    """
        Возвращает ряд заказа по order_id
    :param db: str
    :param table: str
    :param order_id:
    :return object: client
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"SELECT * from {table} WHERE order_id LIKE {order_id};"

        print(query)
        cursor.execute(query)
        try:
            data = cursor.fetchone()
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data


def get_client_id(db=db, table=clients_table, chat_id=None):
    """
        Возвращает ид клиента по переданному chat_id

    :param chat_id: string or int
    :param db: string
    :param table: string
    :return int: client_id
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"SELECT client_id FROM {table} WHERE chat_id='{chat_id}' ;"

        print(query)
        cursor.execute(query)
        data = cursor.fetchone()[0]  # 1st column
        if data is None:
            data = "Не обозначен"
        print(data)
        connector.close()
        return data


def get_client_name(db, table, client_id):
    """
        Возвращает имя клиента по переданному client_id

    :param client_id: string or int
    :param db: string
    :param table: string
    :return int: client_id
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"SELECT client_name FROM {table} WHERE client_id={client_id} ;"

        print(query)
        cursor.execute(query)
        data = cursor.fetchone()[0]  # 1st column
        if data is None:
            data = "Не обозначен"
        print(data)
        connector.close()
        return data


def save_client_number(db, table, client_id, number_id, number, timestamp):
    """
        Пишет номер клиента в соотвествующую таблицу.

    :param number_id:
    :param client_id:
    :param timestamp:
    :param number:
    :param table:
    :param db:
    
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" INSERT INTO {table} VALUES ( '{number_id}', '{client_id}', '{number}', '{timestamp}' ) ;"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_client_current_state(db, table, client_id, current_state):
    """

    :type db: string
    :param table: string
    :param client_id: int
    :param current_state: string
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" UPDATE {table} " \
                f" SET current_state = {current_state}" \
                f" WHERE client_id = {client_id} ;"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_delivery_status(db=db, table=orders_table, order_id=None, delivery_status=None):
    """

    :param order_id:
    :param delivery_status:
    :type db: string
    :param table: string
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" UPDATE {table} " \
                f" SET delivery='{delivery_status}'" \
                f" WHERE order_id LIKE '{order_id}' ;"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_delivery_status_receiver(db=db, table=orders_table, order_id=None, delivery_status=None, status=None):
    """

    :param status:
    :param order_id:
    :param delivery_status:
    :type db: string
    :param table: string
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" UPDATE {table} " \
                f" SET delivery='{delivery_status}', status='{status}' " \
                f" WHERE order_id LIKE '{order_id}' ;"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_order_1st_question(db, table, client_id, order_id, delivery_status, status):
    """
    Сохраняет данные полученные с первого вопроса conversation
    :param order_id:
    :param status:
    :type db: string
    :param table: string
    :param client_id: int
    :param delivery_status:
    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # insert yes/no in {delivery_status} value

        query = f"UPDATE {table} SET delivery_status='{delivery_status}', status={status} " \
                f"WHERE client_id={client_id} AND order_id={order_id}"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_order_1st_question_receiver(db=db, table=orders_table,
                                     brand=None,
                                     order_id=None,
                                     status=None):
    """
    Сохраняет данные полученные с первого вопроса conversation
    :param status:
    :param order_id:
    :param brand:
    :type db: string
    :param table: string
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # insert yes/no in {delivery_status} value

        query = f"UPDATE {table} SET brand_name='{brand}', status='{status}' " \
                f"WHERE order_id LIKE '{order_id}'"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def str_translator(users_input: str) -> str:
    """
        убивает эскейпы для предотвращения sql атаки. У меня почему-то не работают плейсхолдеры,
        так шо такой костыль
    :param users_input: входящая строка от юзера
    :return: измененная строка шобы не еблося
    """
    print(users_input)
    table = str.maketrans("", "", "!@#$%^&*_+|+\/:;[]{}()<>")
    try:
        users_input = users_input.translate(table)
    except AttributeError:
        pass

    vulnerabilities = [
        "and",
        "drop",
        "select",
        "delete",
        "union",
        "create",
        "insert",
        "replace",
    ]

    try:
        for _ in vulnerabilities:
            users_input = users_input.replace(_, "")
    except AttributeError:
        pass

    return users_input


def save_order_2nd_question(db, table, client_id, order_id, brand_name, current_status):
    """

    :type db: string
    :param table: string
    :param client_id: int
    :param order_id:
    :param brand_name:
    :param current_status:

    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # update row in orders table with model name
        print('before translation', brand_name)
        brand_name = str_translator(brand_name)
        print('after translation', brand_name)
        query = f"UPDATE {table} SET brand_name='{brand_name}', status='{current_status}' " \
                f"WHERE client_id={client_id} AND order_id={order_id}"
        print(query)

        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_order_2nd_question_receiver(db=db, table=orders_table,
                                     order_id=None, model_name=None, current_status=None):
    """
    :param model_name:
    :type db: string
    :param table: string
    :param order_id:
    :param current_status:

    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # update row in orders table with model name
        model_name = str_translator(model_name)
        query = f"UPDATE {table} SET model_name='{model_name}', status='{current_status}' " \
                f"WHERE order_id LIKE '{order_id}'"
        print(query)

        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_order_3rd_question(db, table, client_id, order_id, model_name, current_status):
    """

    :type db: string
    :param table: string
    :param client_id: int
    :param order_id:
    :param model_name:
    :param current_status:
    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # update row in orders table with model name
        model_name = str_translator(model_name)
        query = f"UPDATE {table} SET model_name='{model_name}', status='{current_status}' " \
                f"WHERE client_id={client_id} AND order_id={order_id}"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_order_3rd_question_receiver(db=db,
                                     table=orders_table,
                                     order_id=None,
                                     problem=None,
                                     current_status=None
                                     ):
    """

    :param problem:
    :type db: string
    :param table: string
    :param order_id:
    :param current_status:

    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # update row in orders table with model name
        problem = str_translator(problem)
        query = f"UPDATE {table} SET trouble='{problem}', status='{current_status}' " \
                f"WHERE order_id LIKE {order_id}"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_imei(db=db, table=orders_table, order_id=None, imei=None, current_status=None):
    """

    :type db: string
    :param imei:
    :param table: string
    :param order_id:
    :param current_status:

    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # remove escape
        imei = str_translator(imei)

        query = f"UPDATE {table} SET imei='{imei}', status='{current_status}' " \
                f"WHERE order_id LIKE {order_id}"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_comment(db=db, table=orders_table, order_id=None, comment=None, current_status=None):
    """

    :param comment:
    :type db: string
    :param table: string
    :param order_id:
    :param current_status:

    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # remove escape
        comment = str_translator(comment)

        query = f"UPDATE {table} SET comments_receive='{comment}', status='{current_status}' " \
                f"WHERE order_id LIKE {order_id}"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_price(db=db, table=orders_table, order_id=None, price=None, current_status=None):
    """

    :param price:
    :type db: string
    :param table: string
    :param order_id:
    :param current_status:

    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # remove escape
        price = str_translator(price)

        query = f"UPDATE {table} SET price='{price}', status='{current_status}' " \
                f"WHERE order_id LIKE '{order_id}'"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_prepayment(db=db, table=orders_table, order_id=None, prepayment=None, current_status=None):
    """

    :param prepayment:
    :type db: string
    :param table: string
    :param order_id:
    :param current_status:

    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # remove escape
        prepayment = str_translator(prepayment)

        query = f"UPDATE {table} SET status='{current_status}', prepayment='{prepayment}' " \
                f"WHERE order_id LIKE {order_id}"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_order_4th_question(db, table, client_id, order_id, trouble, current_status):
    """


    :type db: string
    :param table: string
    :param client_id: int
    :param order_id:
    :param trouble:
    :param current_status:

    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # update row in orders table with model name
        print('before translation', trouble)
        trouble = str_translator(trouble)
        print('after translation', trouble)
        query = f"UPDATE {table} SET trouble='{trouble}', status='{current_status}' " \
                f"WHERE client_id={client_id} AND order_id={order_id}"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_current_status(db=db, table=orders_table, client_id=None, order_id=None,
                        current_status=None, previous_status=None,
                        status_2nd_delivery=None) -> None:
    """Сохраняет статус заказа с условием текущего статуса

    :param db:
    :param table:
    :param order_id:
    :param client_id:
    :param current_status: новый статус, который присвается текущей строке.
    :type previous_status: меняемый статус
    :param status_2nd_delivery: смена доп статуса

    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # update row in orders table with model name
        if status_2nd_delivery is not None:
            query = f"UPDATE {table} SET status='{current_status}', second_address='{status_2nd_delivery}' " \
                    f"WHERE client_id LIKE '{client_id}' AND order_id LIKE '{order_id}' " \
                    f"AND status LIKE '{previous_status}'; "
        else:
            query = f" UPDATE {table} SET status='{current_status}' " \
                    f" WHERE client_id LIKE '{client_id}'" \
                    f" AND order_id LIKE '{order_id}' AND status LIKE '{previous_status}'; "
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_current_status_cancel(db=db,
                               table=orders_table,
                               order_id=None,
                               status=None,
                               ) -> None:
    """Сохраняет статус заказа с условием текущего статуса

    :param status:
    :param db:
    :param table:
    :param order_id:
    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # update row in orders table with model name
        query = f"UPDATE {table} SET status='{status}'" \
                f"WHERE order_id LIKE '{order_id}'"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_order_trouble(db, table, client_id, order_id, trouble, current_status, previous_status):
    """
    шо делает эта хуета? простите как я написал это говно я не помню

    :type db: str: база
    :param table: str: табличка с данными
    :param client_id: int: идентификатор клиента
    :param order_id: int: идентификатор заказа
    :param trouble: str: строка от пользователя
    :param current_status: str: статус заказа
    :param previous_status:

    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # update row in orders table with model name
        trouble = str_translator(trouble)
        query = f" UPDATE {table} SET trouble='{trouble}', status='{current_status}' " \
                f"WHERE client_id={client_id} AND order_id={order_id} AND status='{previous_status}'; "
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_order_receiver(db=db, table=orders_table, order_id=None, status=None):
    """
    шо делает эта хуета? простите как я написал это говно я не помню

    :param status:
    :type db: str: база
    :param table: str: табличка с данными
    :param order_id: int: идентификатор заказа

    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # update row in orders table with model name
        query = f" UPDATE {table} SET status='{status}'" \
                f" WHERE order_id LIKE '{order_id}'; "
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


# не продумал как это можно применить.


def get_unfinished_orders(db=db, table=orders_table, client_id=None) -> list:
    """
    Получает все заказы от текущего пользователя
    :param db:
    :param table:
    :param client_id:
    :return:
    """
    data = None
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # update row in orders table with model name
        query = f" SELECT * from {table} " \
                f" WHERE status!= "
        cursor.execute(query)
        data = cursor.fetchall()[0]
        connector.commit()
        connector.close()

    finally:
        return data


def get_delivery_status(db=db, table=orders_table, order_id=None) -> int:
    """
        Проверяет состояние заказа на состояние доставки
        Возвращает номер заказа
    :param order_id:
    :param db:
    :param table:
    :return:
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"SELECT delivery FROM {table} " \
                f"WHERE order_id LIKE '{order_id}' ;"

        print(query)
        cursor.execute(query)
        data = cursor.fetchone()[0]  # 1st column
        print(data)
        connector.close()
        return data


def get_clients_current_order(db, table, client_id, status) -> int:
    """
        Возвращает текущий заказ от клиента в опросе к заполнению заказа
    :type db: object
    :param table:
    :param client_id:
    :param status:
    :return result: str
    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()

        query = f"SELECT order_id from {table} WHERE client_id LIKE '{client_id}' and status LIKE '{status}';"

        print(query)
        cursor.execute(query)
        try:
            data = cursor.fetchone()[0]  # 1st column
            print(data)
        except TypeError:
            raise
        else:
            connector.close()
            return data
        finally:
            connector.close()


def get_clients_max_order(db=db, table=orders_table) -> int:
    """
        Возвращает текущий заказ от клиента в опросе к заполнению заказа

    :type db: object
    :param table:
    :return result: str
    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()

        query = f"SELECT MAX(order_id) from {table}"

        print(query)
        cursor.execute(query)
        data = cursor.fetchone()[0]  # 1st column
        print(data)
        connector.close()
        return data


def save_geo(db, table, order_id, position, geo) -> None:
    """

    :type db: string
    :param table: string
    :param order_id:
    :param position:
    :param geo:

    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # update row in orders table with model name
        query = f"INSERT INTO {table} (order_id, position, geo) VALUES ( ''{order_id}', '{position}', \"{geo}\" ) ;"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_address(db, table, order_id, position, address) -> None:
    """

    :type db: string
    :param table: string
    :param order_id:
    :param position:
    :param address:

    """

    address = str_translator(address)
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        # update row in orders table with model name
        address = str_translator(address)
        query = f"INSERT INTO {table} (order_id, position, address_text) VALUES ({order_id}, {position}, '{address}' );"
        cursor.execute(query)
        print(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_selected_role(db=db, table=clients_table, selected_role=None, chat_id=None):
    """

    :param chat_id:
    :param selected_role:
    :param table:
    :type db: object
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"UPDATE {table} SET role='{selected_role}' WHERE chat_id='{chat_id}';"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

        return None


def get_orders_list(db=db, table=orders_table, status=None):
    """
        Search and filter rows by status filter

    :type db: str:  target database
    :type table: str: target table
    :param status: filter status

    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"SELECT * from {table} WHERE status LIKE '{status}';"

        print(query)
        cursor.execute(query)
        try:
            data = cursor.fetchall()
            columns = [description[0] for description in cursor.description]
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns


def get_staff_geo(db=db, table=clients_table, chat_id=None):
    """
        Возвращает ид клиента по переданному chat_id

    :param chat_id: string or int
    :param db: string
    :param table: string
    :return int: client_id
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"SELECT workplace_geo FROM {table} WHERE chat_id='{chat_id}' ;"

        print(query)
        cursor.execute(query)
        data = cursor.fetchone()[0]  # 1st column
        if data is None:
            data = "Не обозначен"
        print(data)
        connector.close()
        return data


def get_order_status(db=db, table=orders_table, order_id=None):
    """
       Возвращает ряд заказа по order_id
    param db: str
    :param table: str
    :param order_id:
    :return object: client
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"SELECT status from {table} WHERE order_id LIKE {order_id};"

        print(query)
        cursor.execute(query)
        try:
            data = cursor.fetchone()
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data


def change_status_order(db=db, table=orders_table, order_id=None, status=None) -> None:
    """ Меняет статус указанного заказа

    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" UPDATE {table} SET status='{status}'" \
                f" WHERE order_id LIKE '{order_id}'; "
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def insert_work_pool(db=db,
                     table=work_table,
                     order_id=None,
                     client_id=None,
                     status=None,
                     timestamp=None):
    """
        Добавляет строку с начатой работой в пул с выполняемыми работами
    :param db:
    :param table:
    :param order_id:
    :param client_id:
    :param status:
    :param client_id:
    :param timestamp:
    :return:
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" INSERT INTO {table} " \
                f" ( order_id, client_id, status, timestamp ) " \
                f" VALUES ( '{order_id}', '{client_id}', '{status}', '{timestamp}' );"
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def get_orders_in_work_list(db=db, table=work_table, client_id=None, status=None):
    """
        Вытаскивает все заказы по идентификатору мастера

    :param client_id:
    :type db: str:  target database
    :type table: str: target table
    :param status: filter status

    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT * from {table} " \
                f" WHERE client_id LIKE '{client_id}' AND status='{status}';"
        print(query)
        cursor.execute(query)
        try:
            data = cursor.fetchall()
            columns = [description[0] for description in cursor.description]
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns


def get_orders_in_work_list_by_order_id(db=db, table=orders_table, order_id=None):
    """
        Ищет один заказ по идентификатору

    :type db: str: target database
    :type table: str: target table
    :type order_id: str: target order
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT * from {table} " \
                f" WHERE order_id LIKE '{order_id}';"
        cursor.execute(query)
        columns = [description[0] for description in cursor.description]
        try:
            data = cursor.fetchone()

        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns


def get_orders_in_work_by_id(db=db, table=work_table, order_id=None):
    """
        Search and filter rows by status filter

    :param order_id: 
    :type db: str:  target database
    :type table: str: target table
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT * from {table} " \
                f" WHERE order_id LIKE '{order_id}';"
        cursor.execute(query)
        try:
            data = cursor.fetchone()
            columns = [description[0] for description in cursor.description]
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns


def get_clients_masters_id(db=db, table=clients_table, chat_id=None):
    """
        Возвращает текущий заказ от клиента в опросе к заполнению заказа

    :param chat_id:
    :type db: object
    :param table:
    :return result: str
    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()

        query = f"SELECT client_id from {table} where chat_id='{chat_id}';"

        print(query)
        cursor.execute(query)
        data = cursor.fetchone()[0]  # 1st column
        print(data)
        connector.close()
        return data


def change_status_work(db=db, table=work_table, order_id=None, status=None):
    """ Меняет статус указанного заказа в таблице работ
    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" UPDATE {table} SET status='{status}'" \
                f" WHERE order_id LIKE '{order_id}' ;"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def insert_delivery_pool(db=db,
                         table=delivery_table,
                         order_id=None,
                         client_id=None,
                         status=None,
                         timestamp=None):
    """
        Добавляет строку с начатой работой в пул с выполняемыми работами
    :param db:
    :param table:
    :param order_id:
    :param client_id:
    :param status:
    :param client_id:
    :param timestamp:
    :return:
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" INSERT INTO {table} " \
                f" ( order_id, client_id, status, timestamp ) " \
                f" VALUES ( '{order_id}', '{client_id}', '{status}', '{timestamp}' );"
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def get_orders_on_delivery_by_status(db=db, table=delivery_table, status=None):
    """
        Search and filter rows by status filter

    :param status:
    :type db: str:  target database
    :type table: str: target table
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT * from {table} " \
                f" WHERE status LIKE '{status}';"
        cursor.execute(query)
        try:
            data = cursor.fetchall()
            columns = [description[0] for description in cursor.description]
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns


def insert_delivered_pool(db=db,
                          table=delivered_table,
                          order_id=None,
                          client_id=None,
                          timestamp=None):
    """
        Добавляет строку с начатой работой в пул с выполняемыми работами
    :param db:
    :param table:
    :param order_id:
    :param client_id:
    :param client_id:
    :param timestamp:
    :return:
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" INSERT or REPLACE INTO {table} " \
                f" ( order_id, client_id, timestamp ) " \
                f" VALUES ( '{order_id}', '{client_id}', '{timestamp}' );"
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def change_status_on_delivery(db=db, table=delivery_table, delivery_id=None, status=None) -> None:
    """
        Меняет статус указанного заказа
    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" UPDATE {table} SET status='{status}'" \
                f" WHERE delivery_id='{delivery_id}' ; "
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_payments_in_db(db=db, table=orders_table, order_id=None, payment=None) -> None:
    """ Меняет статус указанного заказа

    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" UPDATE {table} SET payment='{payment}'" \
                f" WHERE order_id LIKE '{order_id}'; "
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_payments_statement_in_db(db=db, table=table_payment_state, order_id=None, chat_id=None,
                                  message_id=None, message_text=None) -> None:
    """

    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" INSERT or REPLACE INTO {table} (chat_id, order_id, message_id, message_text)" \
                f" VALUES ( '{chat_id}', '{order_id}', '{message_id}', '{message_text}' ) ; "
        try:
            cursor.execute(query)
        except Exception as ee:
            print(ee)
            try:
                query = f" UPDATE {table} " \
                        f" SET order_id='{order_id}', message_id='{message_id}', message_text='{message_text}'" \
                        f" WHERE chat_id='{chat_id}'; "
                cursor.execute(query)
            except Exception as e:
                raise e

        connector.commit()
        connector.close()

    finally:
        return None


def get_order_id_on_payment(db=db, table=table_payment_state, chat_id=None):
    """
        Search and filter rows by status filter

    :param chat_id:
    :type db: str:  target database
    :type table: str: target table
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()

        query = f"SELECT order_id, message_id, message_text from {table} WHERE chat_id='{chat_id}';"
        print(query)
        cursor.execute(query)
        try:
            data = cursor.fetchone()
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data


def get_delivery_id_by_order(db=db, table=delivery_table, order_id=None) -> int:
    """
        Проверяет состояние заказа на состояние доставки.
        Возвращает номер заказа
    :param order_id:
    :param db:
    :param table:
    :return:
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT delivery_id FROM {table} " \
                f" WHERE order_id LIKE {order_id} and status='on_delivery' ;"

        print(query)
        cursor.execute(query)
        data = cursor.fetchone()[0]  # 1st column
        print(data)
        connector.close()
        return data


def get_order_received_payment(db=db, table=orders_table, order_id=None):
    """
		Возвращает ряд заказа по order_id

    :param db: str
    :param table: str
    :param order_id:
    :return object: client
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"SELECT payment from {table} WHERE order_id LIKE '{order_id}';"

        print(query)
        cursor.execute(query)
        try:
            data = cursor.fetchone()[0]
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data


def get_orders_in_work_list_by_status(db=db, table=orders_table, status=None):
    """
        Ищет один заказ по идентификатору

    :param status:
    :type db: str: target database
    :type table: str: target table
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT * from {table} " \
                f" WHERE status='{status}';"
        cursor.execute(query)
        columns = [description[0] for description in cursor.description]
        try:
            data = cursor.fetchall()

        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns


def delete_in_work_by_status(db=db,
                             table=work_table,
                             work_id=None,
                             ):
    """
            Добавляет строку с начатой работой в пул с выполняемыми работами
        :param work_id:
        :param db:
        :param table:
        :return:
        """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" DELETE FROM {table} " \
                f" WHERE work_id='{work_id}' ;"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


table_comment_statements = "comments_statements"


def save_comments_statement_in_db(db=db, table=table_comment_statements, order_id=None, chat_id=None,
                                  message_id=None, message_text=None) -> None:
    """
        Сохраняет промежуточное состояние chat_id в диалоге с изменением статуса этого заказа

    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" INSERT or REPLACE INTO {table} (chat_id, order_id, message_id, message_text)" \
                f" VALUES ( '{chat_id}', '{order_id}', '{message_id}', '{message_text}' ) ; "
        print(query)
        try:
            cursor.execute(query)

        except Exception as ee:
            raise ee

        connector.commit()
        connector.close()

    finally:
        return None


def get_order_id_on_commenting(db=db, table=table_comment_statements, chat_id=None):
    """
        Search and filter rows by status filter

    :param chat_id:
    :type db: str:  target database
    :type table: str: target table
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()

        query = f"SELECT order_id, message_id, message_text from {table} WHERE chat_id='{chat_id}';"
        print(query)
        cursor.execute(query)
        try:
            data = cursor.fetchone()
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data


def save_comments_in_work_db(db=db, table=work_table, order_id=None, comment=None) -> None:
    """ Меняет статус указанного заказа

    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" UPDATE {table} SET users_status='{comment}'" \
                f" WHERE order_id LIKE '{order_id}'; "
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_clients_function_db(db=db, table=clients_table, chat_id=None,
                             selected_function=None, params=None) -> None:
    """
        Сохраняет промежуточное состояние chat_id в диалоге с изменением статуса этого заказа.

    :param db: указанная база данных
    :param table: таблица бд
    :param chat_id: chat_id клиента, которому сохраняется указанная функция
    :param selected_function: выбранная функция
    :param params: параметры выбранной функции
    :return: None
    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" UPDATE {table} " \
                f" SET selected_function='{selected_function}', " \
                f" params='{params}' WHERE chat_id='{chat_id}';"
        print(query)
        try:
            cursor.execute(query)

        except Exception as ee:
            raise ee

        connector.commit()
        connector.close()

    finally:
        return None


def get_selected_function(db=db, table=clients_table, chat_id=None):
    """
        Search and filter rows by status filter

    :param chat_id:
    :type db: str:  target database
    :type table: str: target table
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()

        query = f"SELECT selected_function, params from {table} WHERE chat_id='{chat_id}';"
        print(query)
        cursor.execute(query)
        try:
            data = cursor.fetchone()
            print(data)
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data


def save_orders_users_status_db(db=db, table=work_table, order_id=None, users_status=None) -> None:
    """
        Сохраняет users_status этого заказа

    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" UPDATE {table} " \
                f" SET users_status='{users_status}' " \
                f" WHERE order_id LIKE '{order_id}';"
        print(query)
        try:
            cursor.execute(query)

        except Exception as ee:
            raise ee

        connector.commit()
        connector.close()

    finally:
        return None


def get_order_in_work_by_order_id(db=db, table=work_table, order_id=None):
    """
        Ищет один заказ по идентификатору и возвращает пользовательский статус
        TODO: переименовать

    :param order_id:
    :type db: str: target database
    :type table: str: target table
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT users_status from {table} " \
                f" WHERE order_id LIKE '{order_id}';"
        try:
            cursor.execute(query)
        except sqlite3.OperationalError:
            connector.close()
            return None
        try:
            data = cursor.fetchone()[0]
            print(data)

        except OperationalError:
            connector.close()
            return None
        except TypeError:
            connector.close()
            return "Запись в таблице выполняемых работ не найдена"
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data


# добавить иморт таблицы для деталей

from settings import DATABASE_TABLE_DETAILS_NAME as table_details


def save_phone_number_detail_receiver(db=db, table=table_details,
                                      client_id=None, phone_number=None, status=None):
    """ сохраняет имя клиента на приемке
    :param client_id:
    :type db: object
    :param phone_number:
    :param table:
    :param status:
    """
    timestamp = time()
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"INSERT INTO {table} (phone_number, client_id, status, timestamp) " \
                f"VALUES ( '{phone_number}', '{client_id}', '{status}', {timestamp}) "
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_client_name_detail_receiver(db=db, table=table_details,
                                     name=None,
                                     detail_id=None,
                                     status=None):
    """ сохраняет имя клиента на приемке
    :param detail_id:
    :type db: object
    :param name:
    :param table:
    :param status:
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        name = str_translator(name)
        query = f" UPDATE {table} " \
                f" SET client_name='{name}', status='{status}' " \
                f" WHERE detail_id LIKE '{detail_id}' ;"
        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def save_detail_name_receiver(db=db, table=table_details,
                              detail_id=None, detail_name=None, status=None, ) -> None:
    """
        Сохраняет users_status этого заказа

    """
    query = f" UPDATE {table} " \
            f" SET detail_name='{detail_name}', status='{status}' " \
            f" WHERE detail_id LIKE '{detail_id}';"
    print(query)
    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        cursor.close()
        connector.commit()

    return None


def get_current_detail_id(db=db, table=table_details, client_id=None, status=None):
    """
           Получает идентификатор текущего заполнения детали
        """
    query = f" SELECT detail_id from {table} " \
            f" WHERE client_id LIKE '{client_id}' AND status LIKE '{status}' "
    print(query)
    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        data = cursor.fetchone()[0]
        cursor.close()
        connector.commit()

    return data


def save_detail_commentary_receiver(db=db, table=table_details,
                                    detail_id=None, detail_commentary=None, status=None) -> None:
    """
        Сохраняет комментарий к детали

    """
    query = f" UPDATE {table} " \
            f" SET detail_text='{detail_commentary}', status='{status}' " \
            f" WHERE detail_id LIKE '{detail_id}';"
    print(query)
    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        cursor.close()
        connector.commit()

    return None


def get_unfinished_order(db=db, table=orders_table, statuses=None, client_id=None):
    query = f" SELECT order_id FROM {table} WHERE ( "
    last_status = len(statuses)
    counter = 0
    for _ in statuses:
        counter += 1
        if counter == last_status:
            status = f" status='{_}' "
            query += status
        else:
            status = f" status='{_}' OR "
            query += status

    query += f" ) AND client_id={client_id}"

    print(query)
    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        try:
            data = cursor.fetchone()[0]
        except TypeError:
            data = None
        cursor.close()
        connector.commit()

    return data


def flush_unfinished_order(db=db, table=orders_table, order_id=None):
    query = f" DELETE FROM {table} " \
            f" WHERE order_id LIKE '{order_id}' ; "

    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        cursor.close()
        connector.commit()

    return None


def save_detail_prepayment_receiver(db=db, table=table_details,
                                    detail_id=None, prepayment_text=None, status=None) -> None:
    """
        Сохраняет комментарий к детали

    """
    query = f" UPDATE {table} " \
            f" SET prepayment='{prepayment_text}', status='{status}' " \
            f" WHERE detail_id='{detail_id}';"
    print(query)
    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        cursor.close()
        connector.commit()

    return None


def get_details_list_by_status(db=db, table=table_details, status=None):
    """
        Ищет один заказ по идентификатору

    :param status:
    :type db: str: target database
    :type table: str: target table
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT * from {table} " \
                f" WHERE status='{status}';"
        cursor.execute(query)
        columns = [description[0] for description in cursor.description]
        try:
            data = cursor.fetchall()

        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns


def get_detail_received_payment(db=db, table=table_details, detail_id=None):
    """
        Возвращает ряд заказа по order_id

    :param detail_id: 
    :param db: str
    :param table: str
    :return object: client
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f"SELECT payment from {table} WHERE detail_id='{detail_id}';"

        print(query)
        cursor.execute(query)
        try:
            data = cursor.fetchone()[0]
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data


table_details_payment_state = "payment_detail_statement"


def save_payments_detail_statement_in_db(db=db, table=table_details_payment_state, detail_id=None, chat_id=None,
                                         message_id=None, message_text=None) -> None:
    """

    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" INSERT or REPLACE INTO {table} (chat_id, detail_id, message_id, message_text)" \
                f" VALUES ( '{chat_id}', '{detail_id}', '{message_id}', '{message_text}' ) ; "
        try:
            cursor.execute(query)
        except Exception as ee:
            raise ee

        connector.commit()
        connector.close()

    finally:
        return None


def save_details_finished_db(db=db, table=table_details, detail_id=None, status='finished') -> None:
    """
        Сохраняет users_status этого заказа

    """

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" UPDATE {table} " \
                f" SET status='{status}' " \
                f" WHERE detail_id='{detail_id}';"
        print(query)
        try:
            cursor.execute(query)

        except Exception as ee:
            raise ee

        connector.commit()
        connector.close()

    finally:
        return None


def save_order_1st_question_client(db=db, table=orders_table,
                                   client_id=None,
                                   delivery=None,
                                   status=None):
    """
        Сохраняет данные полученные с первого вопроса conversation
    :param status:
    :type db: string
    :param table: string
    :param client_id: int
    :param delivery:
    """
    timestamp = time()
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()

        query = f" INSERT INTO {table} ( client_id, status, timestamp, delivery ) " \
                f" VALUES ( '{client_id}', '{status}', '{timestamp}', '{delivery}' ) ;"

        print(query)
        cursor.execute(query)
        connector.commit()
        connector.close()

    finally:
        return None


def get_order_in_orders_by_order_id(db=db, table=orders_table, order_id=None):
    """
        Ищет один заказ по идентификатору и возвращает пользовательский статус
        TODO: переименовать

    :param order_id:
    :type db: str: target database
    :type table: str: target table
    :return orders
    """
    order_id = str_translator(order_id)
    print("order_id", order_id)
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT * FROM {table} WHERE order_id LIKE '{order_id}';"
        print("query", query)
        try:
            cursor.execute(query)
        except sqlite3.OperationalError:
            connector.close()
            return None
        try:
            data = cursor.fetchone()
            columns = [description[0] for description in cursor.description]
            print(data)

        except OperationalError:
            connector.close()
            return None
        except TypeError:
            connector.close()
            return "Запись в таблице не найдена"
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns


def get_order_on_delivery_by_order_id(db=db, table=delivery_table, order_id=None):
    """
        Search and filter rows by status filter

    :param order_id:
    :type db: str:  target database
    :type table: str: target table
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT * from {table} " \
                f" WHERE order_id LIKE '{order_id}';"
        cursor.execute(query)
        try:
            data = cursor.fetchone()
            columns = [description[0] for description in cursor.description]
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns


# изменить на возврат столбцов и данных
def get_data_in_work_by_order_id(db=db, table=work_table, order_id=None):
    """
        Ищет один заказ по идентификатору и возвращает пользовательский статус
        TODO: переименовать

    :param order_id:
    :type db: str: target database
    :type table: str: target table
    :return data, columns [*]
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT * from {table} " \
                f" WHERE order_id LIKE '{order_id}';"
        try:
            cursor.execute(query)
        except sqlite3.OperationalError:
            connector.close()
            return None
        try:
            data = cursor.fetchone()
            columns = [description[0] for description in cursor.description]

        except OperationalError:
            connector.close()
            return None
        except TypeError:
            connector.close()
            return "Запись в таблице выполняемых работ не найдена"
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns


def get_users_status_in_work_list_by_order_id(db=db, table=work_table, order_id=None):
    """
        Ищет один заказ по идентификатору

    :type db: str: target database
    :type table: str: target table
    :type order_id: str: target order
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT users_status from {table} " \
                f" WHERE order_id LIKE '{order_id}';"
        cursor.execute(query)
        try:
            data = cursor.fetchone()

        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data


def full_search_sqlite_in_orders(db=db, table=orders_table, request=None):
    """
        Ищет один заказ текстовым поиском по всей таблице, по умолчанию таблица с заказами.
    :param request:
    :type db: str: target database
    :type table: str: target table
    :return orders
    """
    request = str_translator(request)
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT * from {table}( ' {request} '  ) ;"
        print(query)
        cursor.execute(query)
        try:
            data = cursor.fetchall()
            columns = [description[0] for description in cursor.description]
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns


def get_columns(db=db, table=None):
    """
    Получение списка колонок
    :param db: база
    :param table: таблица с колонками
    :return:
    """
    query = f"PRAGMA table_info({table});"
    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        data = cursor.fetchall()

    return [x[1] for x in data]


def create_fts(db=db, new_table_name=None, columns=None):
    """
    Создает таблицу fts5 в указанной базе
    :param db:
    :param new_table_name:
    :param columns:
    :return:
    """
    query = f" CREATE VIRTUAL TABLE {new_table_name} USING FTS5( {','.join(columns)} ); "
    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        connector.commit()


def create_table(db=db, new_table_name=None, columns=None, options=None):
    """
    Создает таблицу fts5 в указанной базе
    :param db:
    :param new_table_name:
    :param columns:
    :param options:
    :return:
    """
    if options:
        options = ', ' + options
    else:
        options = ''

    query = f" CREATE TABLE {new_table_name} ( {','.join(columns)} {options}  ); "
    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        connector.commit()


def copy_data_table_to_table(old_table, new_table):
    """
    Копирует данные с одной таблицы в другую
    :param old_table:
    :param new_table:
    :return:
    """
    query = f" INSERT INTO {new_table} SELECT * FROM {old_table} "
    # вставляем в новую таблицу все данные
    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)


def rename_table(old_name, new_name):
    """
    Переименование таблицы
    :param old_name:
    :param new_name:
    :return:
    """
    query = f" ALTER TABLE {old_name} RENAME TO {new_name} ; "
    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)


def create_ticket(theme=None, chat_id=None, status=None, table=TICKETS_TABLE):
    """
        создает запись в таблице обращений
    :param chat_id: идентификатор чата с которого идет запрос на тикет
    :param theme: Тема (основной вопрос)
    :param status: статус заявки
    :param table: целевая таблица
    :return: номер тикета
    """
    theme = str_translator(str(theme))

    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e

    else:
        cursor = connector.cursor()

        # insert theme
        try:
            query_insert = f" INSERT INTO {table} ( theme, status, chat_id ) " \
                           f" VALUES ('{theme}', '{status}', '{chat_id}' ) "
            print(query_insert)
            cursor.execute(query_insert)
            connector.commit()

        except Exception as e:
            connector.close()
            raise e

        else:
            connector.close()

            return cursor.lastrowid


def get_ticket_chat_id(db=db, table=TICKETS_TABLE, ticket_id=None):
    """
        Получение строки с данными с тикета
    :param db:
    :param table:
    :param ticket_id:
    :return:
    """
    query = f" SELECT chat_id FROM {table} WHERE ticket_id LIKE '{ticket_id}' "
    # вставляем в новую таблицу все данные
    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        try:
            data = cursor.fetchone()[0]
        except TypeError:
            data = None
        return data


def get_file_id_by_name(db=db, name=None):
    """
        Возвращает file_id (telegram) из бд по имени (файла).
        Предназначен только для внутреннего пользования!
    """
    query = f"SELECT file_id from media_pool WHERE media_name LIKE '{name}'; "

    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        try:
            data = cursor.fetchone()[0]  # возвращает всегда кортеж, нулевой индекс - результат запроса
        except TypeError:
            data = None
        return data


def save_file_id(db=db, file_id=None, file_name=None):
    """
    Сохраняет file_id и file_name в mediapool
    :param: db:
    :param: file_id:
    :param: file_name:
    :returns: None
    """
    query = f"INSERT INTO media_pool (media_name, file_id) VALUES ({file_name}, {file_id}); "
    insert_with(db, query)

    return None


def get_client_geo(client_id=None):
    """
        Возвращает прописанный администратором гео клиента.
    Идентификация для бланков
    Args:
        client_id:

    Returns:
        str: geo

    """
    query = f"SELECT workplace_geo from {clients_table} WHERE client_id LIKE {client_id}"

    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        try:
            data = cursor.fetchone()[0]  # возвращает всегда кортеж, нулевой индекс - результат запроса
        except TypeError:
            data = None
        return data


def get_comment_order(db=db, order_id=None):
    """
        Возвращает file_id (telegram) из бд по имени (файла).
        Предназначен только для внутреннего пользования!
    """
    query = f"SELECT comments_receive from orders WHERE order_id LIKE '{order_id}'; "

    with sqlite3.connect(f"{db}.sqlite") as connector:
        cursor = connector.cursor()
        cursor.execute(query)
        try:
            data = cursor.fetchone()[0]  # возвращает всегда кортеж, нулевой индекс - результат запроса
        except TypeError:
            data = None
        return data


def get_order_data_delivery_col(db=db, table=delivery_table, order_id=None):
    """
        Search and filter rows by status filter

    :param order_id:
    :type db: str:  target database
    :type table: str: target table
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT * from {table} " \
                f" WHERE order_id LIKE '{order_id}';"
        cursor.execute(query)
        try:
            data = cursor.fetchone()
            columns = [description[0] for description in cursor.description]
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns


def get_order_data_col(db=db, table=orders_table, order_id=None):
    """
    :param order_id:
    :type db: str:  target database
    :type table: str: target table
    :return orders
    """
    try:
        connector = sqlite3.connect(f"{db}.sqlite")
    except Exception as e:
        raise e
    else:
        cursor = connector.cursor()
        query = f" SELECT * from {table} " \
                f" WHERE order_id LIKE '{order_id}';"
        cursor.execute(query)
        try:
            data = cursor.fetchone()
            columns = [description[0] for description in cursor.description]
        except Exception as e:
            connector.close()
            raise e
        else:
            connector.close()
            return data, columns
