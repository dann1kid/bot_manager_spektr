"""
При создании юзера, вне зависимости от продолжения,
пул должен сразу создавать строку в БДс юзером и незаполненными полями
для закрепления ID, так как она по логике
будет защищена от дубликации айди по принципу лока.

Данный класс заполняет сущность NewOrder, согласно событиям в чате и очередности.
Управляющему классу требуется отдельно проверять какое событие делегировать
этому классу -> TakingOrder

Иерархия :

Управляющий класс -> TakingOrder -> NewOrder

"""


class TakingOrder:

    def __init__(self):
        self.questions = questions()
        # init user app level
        self.client_id = client_id
        # init data for receive from message stream
        self.data = data
        # init key for questions
        self.question = question
        
    def __call__(self):
        return [self.client_id, self.data, self.questions]

    def questions(self):
        from settings import QUESTIONS
        return QUESTIONS

    def iterate_questions(self):
        from NewOrder import NewOrder
        new_order = NewOrder()

        for quest in self.questions:
            new_order.order_id =




