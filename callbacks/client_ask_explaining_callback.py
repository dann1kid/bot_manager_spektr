#!/usr/bin/env python
# -*- coding: utf-8 -*-

from db_manager import save_clients_function_db
from keyboards.TG_keyboards import questions_kbd
from utils.parsers import parse_query
from utils.send_message import send_message_callback


def ask_explaining(update, context):
    """
        Обрабатывает колбек для отправки текста по тикету
    :param update:
    :param context:
    :return:
    """
    # парсим данные
    ticket_id = parse_query(update)
    chat_id = update.effective_chat.id

    # make keyboard
    buttons = [
        'Отмена'
    ]
    reply = questions_kbd(buttons)
    # send message
    send_message_callback(update, context,
                          message="Наберите и отправьте текстовое описание проблемы", reply=reply)

    save_clients_function_db(chat_id=chat_id, selected_function='ticket send explain client',
                             params=f'{ticket_id}')

    return None
