#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from time import time

from telegram import ParseMode

from db_manager import check_in_staff
from db_manager import save_selected_role
from db_manager import change_status_order
from db_manager import insert_work_pool
from db_manager import get_client_id
from db_manager import change_status_on_delivery
from db_manager import insert_delivered_pool
from db_manager import get_order_status

from db_manager import get_order_received_payment
from db_manager import save_clients_function_db
from db_manager import save_details_finished_db

from keyboards.TG_keyboards import questions_kbd
from keyboards.TG_keyboards import hub_keyboard

from utils.send_message import send_message_callback
from utils.delete_message import callback_delete_message

from loguru import logger


def take_payment_detail(update, context):
    """ функция принимает колбек с кнопки на меседже с деталью

        Решил пока просто удалять меседж, потому что путаница происходит из-за колбек-хелла
        Вместе свести трудно поскольку конверсейшнхендлер с колбек ентри
        не вызывается колбеком нормально,
        а перехватывает все колбеки
        """

    query = update.callback_query
    chat_id = update.effective_chat.id

    data = re.findall(r'[\d]{1,15}', query.data)
    function = re.match(r'^\D{1,200}', query.data).group(0)[0:-1]
    detail_id = data[0]

    save_clients_function_db(chat_id=chat_id, selected_function=function, params=detail_id)
    callback_delete_message(update, context)
    send_message_callback(update, context, message=f"Удаление из списка...",)
    save_details_finished_db(detail_id=detail_id)
    save_clients_function_db(chat_id=chat_id, selected_function='', params='')

    return None
