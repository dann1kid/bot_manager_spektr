#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from time import time

from telegram import ParseMode

from db_manager import check_in_staff
from db_manager import save_selected_role
from db_manager import change_status_order
from db_manager import insert_work_pool
from db_manager import get_client_id
from db_manager import change_status_on_delivery
from db_manager import insert_delivered_pool
from db_manager import get_order_status

from db_manager import get_order_received_payment

from keyboards.TG_keyboards import questions_kbd
from keyboards.TG_keyboards import hub_keyboard

from utils.send_message import send_message_callback
from utils.delete_message import callback_delete_message

from loguru import logger


# доделать пул выданных и

def move_delivered_pool_orders_handler(update, context):
    """
        Перемещает выбранный заказ в пул выданных

     """
    query = update.callback_query
    # chat_id исполнителя работы, который нажимает эту кнопку
    chat_id = update.effective_chat.id
    client_id = get_client_id(chat_id=chat_id)

    # выдергивает номер заказа из колбэка
    data = re.findall(r'[\d]{1,15}', query.data)
    print(data)
    order_id, delivery_id = data[0], data[1]

    # тут возникло недопонимание того, что заказ может быть в листе у каждого из участников
    # и два участника могут добавить один и тот же заказ одновременно!
    # и поэтому проверка на статус.
    current_status_order = get_order_status(order_id=order_id)
    if current_status_order[0] != 'on_delivery':
        send_message_callback(update,
                              context,
                              message=f"Заказ №{order_id} уже не доступен!",
                              parse_mode=ParseMode.HTML,
                              )
        callback_delete_message(update, context)
        return None

    # Проверяем значение принятой оплаты
    payment = get_order_received_payment(order_id=order_id)
    print("PAYMENT_TEXT", payment)
    try:
        length = len(payment)
    except TypeError:
        length = 0
    if (payment is None) or (length == 0):
        send_message_callback(update,
                              context,
                              message=f"Заказ <b>№{data[0]}</b> НЕ ОПЛАЧЕН. \n"
                                      f"Определите сумму кнопкой 'ПРИНЯТЬ ОПЛАТУ'",
                              parse_mode=ParseMode.HTML,
                              )
        # callback_delete_message(update, context)
    else:
        # меняет в таблице выдачи статус на "у клиента"
        change_status_on_delivery(delivery_id=delivery_id,
                                  status='at_client',
                                  )

        # меняет статус заказа на finished
        change_status_order(order_id=order_id,
                            status='finished',
                            )

        # добавляет в таблицу с выданными заказами строку
        # в ДАННОМ СЛУЧАЕ клиент_ид == приемщик
        insert_delivered_pool(order_id=order_id,
                              client_id=client_id,
                              timestamp=time(),
                              )

        # отвечает исполнителю о перемещении
        send_message_callback(update,
                              context,
                              message=f"Перемещаю заказ <b>№{data[0]}</b> в пул выданных",
                              parse_mode=ParseMode.HTML,
                              )
        # Удаляет из списка сообщений свободных заказов сообщение с заказом
        callback_delete_message(update, context)

    return None
