from BotManager import BotManager
from settings import TG_BOT_TOKEN
from loguru import logger
import sys

logger.add(sys.stderr, format="{time} {level} {message}", filter="bot_manager_spektr", level="INFO")
logger.add("logs\\file_{time}.log")
logger.add("logs\\file_X.log", retention="10 days")  # Cleanup after some time


# todo переименовать db_tables на db_keys
# todo переименовать db_keys на db_keys_ на db_keys

@logger.catch
def main() -> None:
    try:
        bot = BotManager(TG_BOT_TOKEN)
        bot.start_poll()
    except Exception as e:
        print(e)
        exit(input("Press any key to exit."))


if __name__ == '__main__':
    main()
