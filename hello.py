#!/usr/bin/env python
# -*- coding: utf-8 -*-

# standard libraries
from time import time

# tg libs
from telegram import Update
from User import User

# projects files
from settings import DB_NAME
from settings import DATABASE_TABLE_CLIENTS_FIELDS
from settings import DATABASE_TABLE_CLIENTS_NAME
from db_manager import save_new_user

# interfaces
# задает интерфейс Клиента
from keyboards.TG_keyboards import questions_kbd

# при отсуствии связи chat_id и number показывывает кнопку добавить номер (обсудить)


class Hello:
    """ При нажатии кнопки /start инициирует начальный диалог
    В конструктор передаются параметры:
    :update : пакет апдейта
    :context : пакет контекста
    :client_id : id выданный pool_id.retrieve_id(client_id) управляющим классом
    """

    def __init__(self, update, context, client_id):
        # DB fields
        self.context = context
        self.update = update
        self.client_id = client_id

        # TODO: кнопки сделать для этого интерфейса,
        # добавить кнопку на запрос контакта
        # добавить реплай кнопки
        # self.hello_interface = hello_interface

        # update fields
        self.user = self.update.effective_user
        self.chat_id = self.update.message.chat_id
        self.user_name = self.user.username
        self.user_number = self.user.first_name
        self.current_state = 'new_client'
        self.timestamp = time()
        # self.reply = reply
        
        # инициируемые команды (импорт)

    def start(self):
        # начальный диалог
        # фильтр
        if self.user.is_bot:
            # field for ban bot

            pass

        self.context.bot.send_message(

            chat_id=self.chat_id,
            text=f"Добрый день {self.user_name} \n"
                 f"Для начала работы передайте мне номер для связи (какой вам удобнее)",
            reply_markup=self.contacts_kbd(),
            
        )

        # сохраняем
        self.fulfill()
        
    def check_users_contact(self):
        """Проверяет переданный юзером номер на присуствие в списке номеров у этого chat_id"""
        
        pass

    def fulfill(self):

        save_new_user(db=DB_NAME,
                      table=DATABASE_TABLE_CLIENTS_NAME,
                      client_id=self.client_id,
                      chat_id=self.chat_id,
                      name=self.user_name,
                      current_state=self.current_state,
                      timestamp=self.timestamp,
                      )

    def generate_keyboard(self):
        """
            Чекает права и текущую роль юзера,
            добавляет кнопки к текущему сообщению на их основе
        :return reply:
        """
        # get roles of user
        # must be replaced by generated buttons by rights
        # later brr

        buttons = ["Новый заказ",
                   "Мои заказы",
                   ]
        return questions_kbd(buttons)


    def contacts_kbd(self):
        
        get_user_defined_contact = "Поделится номером"
    
        buttons = [
                   get_user_defined_contact,
                   ]
        return questions_kbd(buttons)