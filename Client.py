from User import User


class Client(User):
    """
    Класс клиента.
    Наследует класс User
    """

    def __init__(self, name, client_id):
        User.__init__(self, name, 'client')
        self.client_id = client_id

    def __repr__(self):
        print(f'{self.client_id}, {self.name}, {self.status}')

    def __str__(self):
        print(f'I\'m {self.client_id}, {self.name}, {self.status}')
