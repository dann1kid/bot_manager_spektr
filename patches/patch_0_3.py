#!/usr/bin/env python
# -*- coding: utf-8 -*-

from db_manager import create_table, copy_data_table_to_table, rename_table, get_columns
import sys
sys.path.append('..')

if __name__ == '__main__':
    table = "tickets"
    columns = ["ticket_id INTEGER UNIQUE", "theme TEXT ", "status TEXT", "chat_id TEXT"]
    options = "PRIMARY KEY('ticket_id' AUTOINCREMENT)"

    try:
        create_table(new_table_name=table, columns=columns, options=options)
    except Exception as e:
        print("Ошибка создания", e)
        pass
    else:
        print("Таблица создана")
